﻿namespace BlogEngine.Core.Data.Models
{
    /// <summary>
    /// Blog settings
    /// </summary>
    public class Settings
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public Settings() { }

        /// <summary>
        /// Seeting Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Posts per page
        /// </summary>
        public int PostsPerPage { get; set; }

        /// <summary>
        /// Theme cookie name used to override default theme for a session
        /// </summary>
        public string ThemeCookieName { get; set; }

        /// <summary>
        /// Blog name in page titles
        /// </summary>
        public bool UseBlogNameInPageTitles { get; set; }

        /// <summary>
        /// Enables related posts
        /// </summary>
        public bool EnableRelatedPosts { get; set; }

        /// <summary>
        /// Enables post ratings
        /// </summary>
        public bool EnableRating { get; set; }

        /// <summary>
        /// Shows post description instead of content
        /// </summary>
        public bool ShowDescriptionInPostList { get; set; }

        /// <summary>
        /// Number of characters in post description
        /// </summary>
        public int DescriptionCharacters { get; set; }

        /// <summary>
        /// Only shows post description for tags and categories
        /// </summary>
        public bool ShowDescriptionInPostListForPostsByTagOrCategory { get; set; }

        /// <summary>
        /// Number of characters for description in tags/category lists
        /// </summary>
        public int DescriptionCharactersForPostsByTagOrCategory { get; set; }

        /// <summary>
        /// Show time stamp
        /// </summary>
        public bool TimeStampPostLinks { get; set; }

        /// <summary>
        /// Show post navigation
        /// </summary>
        public bool ShowPostNavigation { get; set; }

        /// <summary>
        /// Culture
        /// </summary>
        public string Culture { get; set; }

        /// <summary>
        /// Time zone
        /// </summary>
        public double Timezone { get; set; }

        /// <summary>
        /// Removes extensions from urls
        /// </summary>
        public bool RemoveExtensionsFromUrls { get; set; }

        /// <summary>
        /// Sets redirect if file extension not used (for updated blogs)
        /// </summary>
        public bool RedirectToRemoveFileExtension { get; set; }

        /// <summary>
        /// How to handle www sub-domain
        /// </summary>
        public string HandleWwwSubdomain { get; set; }

        /// <summary>
        /// Default desktop theme
        /// </summary>
        public string DesktopTheme { get; set; }

        /// <summary>
        /// Default mobile theme
        /// </summary>
        public string MobileTheme { get; set; }

        // advanced settings
        /// <summary>
        /// Enable HTTP compression
        /// </summary>
        public bool EnableHttpCompression { get; set; }

        /// <summary>
        /// Compress web resources
        /// </summary>
        public bool CompressWebResource { get; set; }

        /// <summary>
        /// Enable open search
        /// </summary>
        public bool EnableOpenSearch { get; set; }
        
        /// <summary>
        /// Require SSL for meta weblog api
        /// </summary>
        public bool RequireSslForMetaWeblogApi { get; set; }

        /// <summary>
        /// Enable error logging
        /// </summary>
        public bool EnableErrorLogging { get; set; }

        /// <summary>
        /// Gallery feed url
        /// </summary>
        public string GalleryFeedUrl { get; set; }

        /// <summary>
        /// EnablePasswordReset
        /// </summary>
        public bool EnablePasswordReset { get; set; }

        /// <summary>
        /// EnableSelfRegistration
        /// </summary>
        public bool EnableSelfRegistration { get; set; }

        /// <summary>
        /// CreateBlogOnSelfRegistration
        /// </summary>
        public bool CreateBlogOnSelfRegistration { get; set; }

        /// <summary>
        /// AllowServerToDownloadRemoteFiles
        /// </summary>
        public bool AllowServerToDownloadRemoteFiles { get; set; }

        /// <summary>
        /// RemoteFileDownloadTimeout
        /// </summary>
        public int RemoteFileDownloadTimeout { get; set; }

        /// <summary>
        ///RemoteMaxFileSize
        /// </summary>
        public int RemoteMaxFileSize { get; set; }

        /// <summary>
        /// SelfRegistrationInitialRole
        /// </summary>
        public string SelfRegistrationInitialRole { get; set; }

        // feed
        /// <summary>
        /// AuthorName
        /// </summary>
        public string AuthorName { get; set; }

        /// <summary>
        /// FeedAuthor
        /// </summary>
        public string FeedAuthor { get; set; }

        /// <summary>
        /// Endorsement
        /// </summary>
        public string Endorsement { get; set; }

        /// <summary>
        /// AlternateFeedUrl
        /// </summary>
        public string AlternateFeedUrl { get; set; }

        /// <summary>
        /// Language
        /// </summary>
        public string Language { get; set; }

        /// <summary>
        /// PostsPerFeed
        /// </summary>
        public int PostsPerFeed { get; set; }

        /// <summary>
        /// EnableEnclosures
        /// </summary>
        public bool EnableEnclosures { get; set; }

        /// <summary>
        /// EnableTagExport
        /// </summary>
        public bool EnableTagExport { get; set; }

        /// <summary>
        /// SyndicationFormat
        /// </summary>
        public string SyndicationFormat { get; set; }

        // email
        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// SmtpServer
        /// </summary>
        public string SmtpServer { get; set; }

        /// <summary>
        /// SmtpServerPort
        /// </summary>
        public int SmtpServerPort { get; set; }

        /// <summary>
        /// SmtpUserName
        /// </summary>
        public string SmtpUserName { get; set; }

        /// <summary>
        /// SmtpPassword
        /// </summary>
        public string SmtpPassword { get; set; }

        /// <summary>
        /// EmailSubjectPrefix
        /// </summary>
        public string EmailSubjectPrefix { get; set; }

        /// <summary>
        /// EnableSsl
        /// </summary>
        public bool EnableSsl { get; set; }

        /// <summary>
        /// SendMailOnComment
        /// </summary>
        public bool SendMailOnComment { get; set; }

        // controls
        /// <summary>
        /// NumberOfRecentPosts
        /// </summary>
        public int NumberOfRecentPosts { get; set; }

        /// <summary>
        /// DisplayCommentsOnRecentPosts
        /// </summary>
        public bool DisplayCommentsOnRecentPosts { get; set; }

        /// <summary>
        /// DisplayRatingsOnRecentPosts
        /// </summary>
        public bool DisplayRatingsOnRecentPosts { get; set; }

        /// <summary>
        /// NumberOfRecentComments
        /// </summary>
        public int NumberOfRecentComments { get; set; }

        /// <summary>
        /// SearchButtonText
        /// </summary>
        public string SearchButtonText { get; set; }

        /// <summary>
        /// SearchCommentLabelText
        /// </summary>
        public string SearchCommentLabelText { get; set; }

        /// <summary>
        /// SearchDefaultText
        /// </summary>
        public string SearchDefaultText { get; set; }

        /// <summary>
        /// EnableCommentSearch
        /// </summary>
        public bool EnableCommentSearch { get; set; }

        /// <summary>
        /// ShowIncludeCommentsOption
        /// </summary>
        public bool ShowIncludeCommentsOption { get; set; }

        /// <summary>
        /// ContactFormMessage
        /// </summary>
        public string ContactFormMessage { get; set; }

        /// <summary>
        /// ContactThankMessage
        /// </summary>
        public string ContactThankMessage { get; set; }

        /// <summary>
        /// ContactErrorMessage
        /// </summary>
        public string ContactErrorMessage { get; set; }

        /// <summary>
        /// EnableContactAttachments
        /// </summary>
        public bool EnableContactAttachments { get; set; }

        /// <summary>
        /// EnableRecaptchaOnContactForm
        /// </summary>
        public bool EnableRecaptchaOnContactForm { get; set; }

        /// <summary>
        /// ErrorTitle
        /// </summary>
        public string ErrorTitle { get; set; }

        /// <summary>
        /// ErrorText
        /// </summary>
        public string ErrorText { get; set; }

        // custom code
        /// <summary>
        /// HtmlHeader
        /// </summary>
        public string HtmlHeader { get; set; }

        /// <summary>
        /// TrackingScript
        /// </summary>
        public string TrackingScript { get; set; }

        // comments
        /// <summary>
        /// DaysCommentsAreEnabled
        /// </summary>
        public int DaysCommentsAreEnabled { get; set; }

        /// <summary>
        /// IsCommentsEnabled
        /// </summary>
        public bool IsCommentsEnabled { get; set; }

        /// <summary>
        /// EnableCommentsModeration
        /// </summary>
        public bool EnableCommentsModeration { get; set; }

        /// <summary>
        /// IsCommentNestingEnabled
        /// </summary>
        public bool IsCommentNestingEnabled { get; set; }

        /// <summary>
        /// IsCoCommentEnabled
        /// </summary>
        public bool IsCoCommentEnabled { get; set; }

        /// <summary>
        /// Avatar
        /// </summary>
        public string Avatar { get; set; }

        /// <summary>
        /// EnablePingBackSend
        /// </summary>
        public bool EnablePingBackSend { get; set; }

        /// <summary>
        /// EnablePingBackReceive
        /// </summary>
        public bool EnablePingBackReceive { get; set; }

        /// <summary>
        /// EnableTrackBackSend
        /// </summary>
        public bool EnableTrackBackSend { get; set; }

        /// <summary>
        /// EnableTrackBackReceive
        /// </summary>
        public bool EnableTrackBackReceive { get; set; }

        /// <summary>
        /// ThumbnailServiceApi
        /// </summary>
        public string ThumbnailServiceApi { get; set; }

        /// <summary>
        /// CommentsPerPage
        /// </summary>
        public int CommentsPerPage { get; set; }

        /// <summary>
        /// EnableCountryInComments
        /// </summary>
        public bool EnableCountryInComments { get; set; }

        /// <summary>
        /// EnableWebsiteInComments
        /// </summary>
        public bool EnableWebsiteInComments { get; set; }

        /// <summary>
        /// ShowLivePreview
        /// </summary>
        public bool ShowLivePreview { get; set; }

        /// <summary>
        /// UseDisqus
        /// </summary>
        public bool UseDisqus { get; set; }

        /// <summary>
        /// DisqusDevMode
        /// </summary>
        public bool DisqusDevMode { get; set; }

        /// <summary>
        /// DisqusAddCommentsToPages
        /// </summary>
        public bool DisqusAddCommentsToPages { get; set; }

        /// <summary>
        /// DisqusWebsiteName
        /// </summary>
        public string DisqusWebsiteName { get; set; }

        // custom filters
        /// <summary>
        /// CommentWhiteListCount
        /// </summary>
        public int CommentWhiteListCount { get; set; }

        /// <summary>
        /// CommentBlackListCount
        /// </summary>
        public int CommentBlackListCount { get; set; }

        /// <summary>
        /// AddIpToWhitelistFilterOnApproval
        /// </summary>
        public bool AddIpToWhitelistFilterOnApproval { get; set; }

        /// <summary>
        /// TrustAuthenticatedUsers
        /// </summary>
        public bool TrustAuthenticatedUsers { get; set; }

        /// <summary>
        /// BlockAuthorOnCommentDelete
        /// </summary>
        public bool BlockAuthorOnCommentDelete { get; set; }

        /// <summary>
        /// AddIpToBlacklistFilterOnRejection
        /// </summary>
        public bool AddIpToBlacklistFilterOnRejection { get; set; }
    }
}
