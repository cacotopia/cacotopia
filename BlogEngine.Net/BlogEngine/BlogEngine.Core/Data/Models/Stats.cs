﻿namespace BlogEngine.Core.Data.Models
{
    public class Stats
    {
        /// <summary>
        /// PublishedPostsCount
        /// </summary>
        public int PublishedPostsCount { get; set; }

        /// <summary>
        /// DraftPostsCount
        /// </summary>
        public int DraftPostsCount { get; set; }

        /// <summary>
        /// PublishedPagesCount
        /// </summary>
        public int PublishedPagesCount { get; set; }

        /// <summary>
        /// DraftPagesCount
        /// </summary>
        public int DraftPagesCount { get; set; }

        /// <summary>
        /// PublishedCommentsCount
        /// </summary>
        public int PublishedCommentsCount { get; set; }

        /// <summary>
        /// UnapprovedCommentsCount
        /// </summary>
        public int UnapprovedCommentsCount { get; set; }

        /// <summary>
        /// SpamCommentsCount
        /// </summary>
        public int SpamCommentsCount { get; set; }

        /// <summary>
        /// CategoriesCount
        /// </summary>
        public int CategoriesCount { get; set; }

        /// <summary>
        /// TagsCount
        /// </summary>
        public int TagsCount { get; set; }

        /// <summary>
        /// UsersCount
        /// </summary>
        public int UsersCount { get; set; }
    }
}
