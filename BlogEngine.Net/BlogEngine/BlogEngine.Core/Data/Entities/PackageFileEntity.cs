﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogEngine.Core.Data.Entities
{
    public class PackageFileEntity
    {
        public virtual string PackageId { get; set; }

        public virtual int FileOrder { get; set; }

        public virtual string FilePath { get; set; }

        public virtual bool IsDicreatory { get; set; }
    }
}
