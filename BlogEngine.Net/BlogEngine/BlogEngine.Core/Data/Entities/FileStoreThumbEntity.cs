﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogEngine.Core.Data.Entities
{
    public class FileStoreThumbEntity
    {
        public virtual Guid ThumbnailId { get; set; }

        public virtual Guid FileId { get; set; }

        public virtual int Size { get; set; }

        public virtual byte[] Contents { get; set; }
    }
}
