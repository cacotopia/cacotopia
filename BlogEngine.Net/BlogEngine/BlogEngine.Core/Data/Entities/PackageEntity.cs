﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogEngine.Core.Data.Entities
{
    public class PackageEntity
    {
        public virtual string PackageId { get; set; }

        public virtual string Version { get; set; }
    }
}
