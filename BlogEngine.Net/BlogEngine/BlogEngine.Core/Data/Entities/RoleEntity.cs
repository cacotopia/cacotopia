﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogEngine.Core.Data.Entities
{
    public class RoleEntity
    {
        public virtual int RoleID { get; set; }

        public virtual Guid BlogID { get; set; }

        public virtual string Role { get; set; }
    }
}
