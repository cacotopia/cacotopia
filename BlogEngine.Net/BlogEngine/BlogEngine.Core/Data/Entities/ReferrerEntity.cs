﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogEngine.Core.Data.Entities
{
    public class ReferrerEntity
    {
        public virtual int ReferrerRowId { get; set; }

        public virtual Guid BlogID { get; set; }

        public virtual Guid ReferrerID { get; set; }

        public virtual string ReferredUrl { get; set; }

        public virtual int ReferrerCount { get; set; }

        public virtual string Url { get; set; }

        public virtual bool? IsSpam { get; set; }
    }
}
