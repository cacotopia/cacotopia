﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogEngine.Core.Data.Entities
{
    public class PageEntity
    {
        public virtual int PageRowId { get; set; }

        public virtual Guid BlogId { get;set;}

        public virtual Guid PageId { get; set; }

        public virtual string Title { get; set; }

        public virtual string Description { get; set; }

        public virtual string PageContents { get; set; }

        public virtual string Keywords { get; set; }

        public virtual DateTime? DateCreated { get; set; }

        public virtual DateTime? DateModified { get; set; }

        public virtual bool? IsPublished { get; set; }

        public virtual bool? IsFrontPage { get; set; }

        public virtual Guid Parent { get; set; }

        public virtual bool? ShowInList { get; set; }

        public virtual string Slug { get; set; }

        public virtual bool IsDeleted { get; set; }
    }
}
