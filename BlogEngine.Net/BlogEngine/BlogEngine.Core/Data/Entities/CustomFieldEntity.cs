﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlogEngine.Core.Data.Entities
{
    public class CustomFieldEntity
    {
        public virtual string CustomType { get; set; }

        public virtual string ObjectId { get; set; }

        public virtual Guid BlogId { get; set; }

        public virtual string Key { get; set; }

        public virtual string Value { get; set; }

        public virtual string Attribute { get; set; }

    }
}
