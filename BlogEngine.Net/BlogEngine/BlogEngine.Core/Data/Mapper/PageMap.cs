﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using BlogEngine.Core.Data.Entities;

namespace BlogEngine.Core.Data.Mapper
{
    /// <summary>
    /// 
    /// </summary>
    public class PageMap :ClassMap<PageEntity>
    {
        /// <summary>
        /// 
        /// </summary>
        public PageMap() 
        {
            Id(x=>x.PageRowId);
            Map(x=>x.BlogId);
            Map(x=>x.DateCreated);
            Map(x=>x.DateModified);
            Map(x=>x.Description);
            Map(x=>x.IsDeleted);
            Map(x=>x.IsFrontPage);
            Map(x=>x.IsPublished);
            Map(x=>x.Keywords);
            Map(x=>x.PageContents);
            Map(x=>x.PageId);
            Map(x=>x.Parent);
            Map(x=>x.ShowInList);
            Map(x=>x.Title);
            Map(x=>x.Slug);
        }
    }
}
