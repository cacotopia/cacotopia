﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using BlogEngine.Core.Data.Entities;

namespace BlogEngine.Core.Data.Mapper
{
    /// <summary>
    /// 
    /// </summary>
    public class UserMap :ClassMap<UserEntity>
    {
        public UserMap() 
        {
            Id(x=>x.UserID);
            Map(x=>x.UserName);
            Map(x=>x.BlogID);
            Map(x=>x.EmailAddress);
            Map(x=>x.LastLogintTime);
            Map(x=>x.Password);           
        }
    }
}
