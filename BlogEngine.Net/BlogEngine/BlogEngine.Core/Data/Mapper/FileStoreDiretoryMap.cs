﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using BlogEngine.Core.Data.Entities;

namespace BlogEngine.Core.Data.Mapper
{
    /// <summary>
    /// 
    /// </summary>
    public class FileStoreDiretoryMap:ClassMap<FileStoreDirectoryEntity>
    {
        public FileStoreDiretoryMap() 
        {
            Id(x=>x.Id);
            Map(x=>x.BlogID);
            Map(x=>x.CreateDate);
            Map(x=>x.FullPath);
            Map(x=>x.LastAccess);
            Map(x=>x.LastModify);
            Map(x=>x.Name);
            Map(x=>x.ParentID);
        }
    }
}
