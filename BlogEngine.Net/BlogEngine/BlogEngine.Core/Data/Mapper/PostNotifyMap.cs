﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using BlogEngine.Core.Data.Entities;

namespace BlogEngine.Core.Data.Mapper
{
    /// <summary>
    /// 
    /// </summary>
    public class PostNotifyMap :ClassMap<PostNotifyEntity>
    {
        /// <summary>
        /// 
        /// </summary>
        public PostNotifyMap() 
        {
            Id(x=>x.PostNotifyID);
            Map(x=>x.BlogID);
            Map(x=>x.NotifyAddress);
            Map(x=>x.PostID);           
        }
    }
}
