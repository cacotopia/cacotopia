﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using BlogEngine.Core.Data.Entities;

namespace BlogEngine.Core.Data.Mapper
{
    /// <summary>
    /// 
    /// </summary>
    public class BlogMap :ClassMap<BlogEntity>
    {
        /// <summary>
        /// 
        /// </summary>
        public BlogMap() 
        {
            Map(x => x.BlogId);
            Map(x => x.BlogName);
            Map(x => x.BlogRowId);
            Map(x => x.HostName);
            Map(x => x.IsActive);
            Map(x=>x.IsAnyTextBeforeHostnameAccepted);
            Map(x => x.IsPrimary);
            Map(x => x.IsSiteAggregation);
            Map(x => x.StorageContainerName);
            Map(x => x.VirtualPath);          
        }
    }
}
