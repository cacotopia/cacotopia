﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using BlogEngine.Core.Data.Entities;

namespace BlogEngine.Core.Data.Mapper
{
    /// <summary>
    /// 
    /// </summary>
    public class DataStoreSettingMap:ClassMap<DataStoreSettingEntity>
    {
        /// <summary>
        /// 
        /// </summary>
        public DataStoreSettingMap() 
        {
            Id(x=>x.DataStoreSettingRowId);
            Map(x=>x.BlogId);
            Map(x=>x.ExtensionId);
            Map(x=>x.ExtensionType);
            Map(x=>x.Settings);
        }
    }
}
