﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using BlogEngine.Core.Data.Entities;

namespace BlogEngine.Core.Data.Mapper
{
    /// <summary>
    /// 
    /// </summary>
    public class PackageFileMap :ClassMap<PackageFileEntity>
    {
        /// <summary>
        /// 
        /// </summary>
        public PackageFileMap() 
        {
            Id(x=>x.PackageId);
            Map(x=>x.FileOrder);
            Map(x=>x.FilePath);
            Map(x=>x.IsDicreatory);           
        }
    }
}
