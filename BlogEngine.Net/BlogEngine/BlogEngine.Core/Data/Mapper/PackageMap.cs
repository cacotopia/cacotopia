﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using BlogEngine.Core.Data.Entities;

namespace BlogEngine.Core.Data.Mapper
{
    /// <summary>
    /// 
    /// </summary>
    public class PackageMap :ClassMap<PackageEntity>
    {
        /// <summary>
        /// 
        /// </summary>
        public PackageMap() 
        {
            Id(x=>x.PackageId);
            Map(x=>x.Version);
        }
    }
}
