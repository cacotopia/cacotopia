﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using BlogEngine.Core.Data.Entities;

namespace BlogEngine.Core.Data.Mapper
{
    /// <summary>
    /// 
    /// </summary>
    public class CategoryMap:ClassMap<CategoryEntity>
    {
        /// <summary>
        /// 
        /// </summary>
        public CategoryMap() 
        {
            Map(x => x.BlogID);
            Map(x => x.CategoryID);
            Map(x => x.CategoryName);
            Map(x => x.Description);
            Map(x => x.ParentID);
            Map(x => x.CategoryRowID);
        }
    }
}
