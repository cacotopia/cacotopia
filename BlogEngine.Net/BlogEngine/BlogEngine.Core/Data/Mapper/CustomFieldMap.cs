﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using BlogEngine.Core.Data.Entities;

namespace BlogEngine.Core.Data.Mapper
{
    /// <summary>
    /// 
    /// </summary>
    public class CustomFieldMap :ClassMap<CustomFieldEntity>
    {
        /// <summary>
        /// 
        /// </summary>
        public CustomFieldMap() 
        {
            Map(x => x.BlogId);
            Map(x => x.CustomType);
            Map(x => x.Key);
            Map(x => x.ObjectId);
            Map(x => x.Attribute);
            Map(x => x.Value);
        }
    }
}
