﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using BlogEngine.Core.Data.Entities;

namespace BlogEngine.Core.Data.Mapper
{
    /// <summary>
    /// 
    /// </summary>
    public class BlogRollItemMap :ClassMap<BlogRollItemEntity>
    {
        /// <summary>
        /// 
        /// </summary>
        public BlogRollItemMap() 
        {
            Map(x => x.BlogId);
            Map(x => x.BlogRollId);
            Map(x=>x.BlogRollRowId);
            Map(x => x.Description);
            Map(x=>x.FeedUrl);
            Map(x => x.SortIndex);
            Map(x => x.Title);
            Map(x=>x.Xfn);
        }
    }
}
