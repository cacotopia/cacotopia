﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using BlogEngine.Core.Data.Entities;

namespace BlogEngine.Core.Data.Mapper
{
    /// <summary>
    /// 
    /// </summary>
    public class FileStoreFileMap :ClassMap<FileStoreFileEntity>
    {
        /// <summary>
        /// 
        /// </summary>
        public FileStoreFileMap() 
        {
            Id(x=>x.FileID);
            Map(x=>x.Contents);
            Map(x=>x.CreateDate);
            Map(x=>x.FullPath);
            Map(x=>x.LastAccess);
            Map(x=>x.LastModify);
            Map(x=>x.Name);
            Map(x=>x.ParentDirectoryID);
            Map(x=>x.Size);
        }
    }
}
