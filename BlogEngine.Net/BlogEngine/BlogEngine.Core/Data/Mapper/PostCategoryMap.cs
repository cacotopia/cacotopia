﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using BlogEngine.Core.Data.Entities;

namespace BlogEngine.Core.Data.Mapper
{
    /// <summary>
    /// 
    /// </summary>
    public class PostCategoryMap:ClassMap<PostCategoryEntity>
    {
        /// <summary>
        /// 
        /// </summary>
        public PostCategoryMap() 
        {
            Id(x=>x.PostCategoryID);
            Map(x=>x.BlogID);
            Map(x=>x.CategoryID);
            Map(x=>x.PostID);
        }
    }
}
