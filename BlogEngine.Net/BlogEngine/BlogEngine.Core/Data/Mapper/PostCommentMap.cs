﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using BlogEngine.Core.Data.Entities;

namespace BlogEngine.Core.Data.Mapper
{
    /// <summary>
    /// 
    /// </summary>
    public class PostCommentMap :ClassMap<PostCommentEntity>
    {
        public PostCommentMap() 
        {
            Id(x=>x.PostCommentRowID);
           
            Map(x=>x.Author);
            Map(x=>x.Avatar);
            Map(x=>x.BlogID);
            Map(x=>x.CommentDate);
            Map(x=>x.Country);
            Map(x=>x.Email);
            Map(x=>x.Ip);
            Map(x=>x.IsApproved);
            Map(x=>x.IsDeleted);
            Map(x=>x.IsSpam);
            Map(x=>x.ModeratedBy);
            Map(x=>x.ParentCommentID);
            Map(x=>x.PostCommentID);
            Map(x => x.PostID);
            Map(x=>x.Website);
        }
    }
}
