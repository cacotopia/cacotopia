﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using BlogEngine.Core.Data.Entities;

namespace BlogEngine.Core.Data.Mapper
{
    /// <summary>
    /// 
    /// </summary>
    public class PingServiceMap :ClassMap<PingServiceEntity>
    {
        public PingServiceMap() 
        {
            Id(x=>x.PingServiceID);
            Map(x=>x.BlogID);
            Map(x=>x.Link);
        }
    }
}
