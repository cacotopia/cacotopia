﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using BlogEngine.Core.Data.Entities;

namespace BlogEngine.Core.Data.Mapper
{
    /// <summary>
    /// 
    /// </summary>
    public class FileStoreThumbMap:ClassMap<FileStoreThumbEntity>
    {
        /// <summary>
        /// 
        /// </summary>
        public FileStoreThumbMap() 
        {
            Id(x=>x.ThumbnailId);
            Map(x=>x.FileId);
            Map(x=>x.Contents);
            Map(x=>x.Size);           
        }
    }
}
