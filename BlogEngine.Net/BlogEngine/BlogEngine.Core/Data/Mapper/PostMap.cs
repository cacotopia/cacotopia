﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using BlogEngine.Core.Data.Entities;

namespace BlogEngine.Core.Data.Mapper
{
    /// <summary>
    /// 
    /// </summary>
    public class PostMap:ClassMap<PostEntity>
    {
        public PostMap() 
        {
            Id(x=>x.PostRowID);
            Map(x=>x.Author);
            Map(x=>x.BlogID);
            Map(x=>x.DateCreated);
            Map(x=>x.DateModified);
            Map(x=>x.Description);
            Map(x=>x.IsCommentEnabled);
            Map(x=>x.IsDeleted);
            Map(x=>x.IsPublished);
            Map(x=>x.PostContent);
            Map(x=>x.PostID);
            Map(x=>x.Raters);
            Map(x=>x.Rating);
            Map(x=>x.Slug);
            Map(x=>x.Title);
        }
    }
}
