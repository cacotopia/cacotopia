﻿#region Using

using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Configuration;

using Cassandra;
using Cassandra.Data;

#endregion


namespace BlogEngine.Core.Providers.CassandraProvider
{
    /// <summary>
    /// CassandraSessionHelper
    /// </summary>
    public static class CassandraSessionHelper
    {
        private const string ClusterLocationAppSettingsKey = "CassandraClusterLocation";
        private const string CassandraKeyspaceSettingsKey = "CassandraKeyspace";

        private static string clusterLocation;
        private static string keyspace;

        static CassandraSessionHelper() 
        {
            //
            if (string.IsNullOrEmpty(clusterLocation)) 
            {
                clusterLocation = WebConfigurationManager.AppSettings[ClusterLocationAppSettingsKey];
            }
            if (string.IsNullOrEmpty(keyspace)) 
            {
                keyspace = WebConfigurationManager.AppSettings[CassandraKeyspaceSettingsKey];
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static ISession CreateSession() 
        {
            // Start a cluster builder for connecting to Cassandra
            Builder builder = Cluster.Builder();

            // Allow multiple comma delimited locations to be specified in the configuration
            string[] locations = clusterLocation.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(l => l.Trim()).ToArray();
            foreach (string location in locations)
            {
                string[] hostAndPort = location.Split(new[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                if (hostAndPort.Length == 1)
                {
                    // Just an IP address or host name
                    builder = builder.AddContactPoint(hostAndPort[0]);
                }
                else if (hostAndPort.Length == 2)
                {
                    // IP Address plus host name
                    var ipEndPoint = new IPEndPoint(IPAddress.Parse(hostAndPort[0]), int.Parse(hostAndPort[1]));
                    builder = builder.AddContactPoint(ipEndPoint);
                }
                else
                {
                    throw new InvalidOperationException(string.Format("Unable to parse Cassandra cluster location '{0}' from configuration.", location));
                }
            }

            // Use the Cluster builder to create a cluster
            Cluster cluster = builder.Build();

            // Use the cluster to connect a session to the appropriate keyspace
            ISession session;
            try
            {
                session = cluster.Connect(keyspace);
            }
            catch (Exception ex)
            {
                //Logger.Error(e, "Exception while connecting to '{Keyspace}' using '{Hosts}'", Keyspace, clusterLocation);
                throw ex;
            }
            return session;
        }
    }
}
