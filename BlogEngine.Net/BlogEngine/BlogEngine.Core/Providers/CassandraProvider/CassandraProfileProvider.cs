﻿#region Using

using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Configuration.Provider;
using System.Data;
using System.Data.Odbc;
using System.Diagnostics;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;
using System.Web.Profile;
using Cassandra;

#endregion

namespace BlogEngine.Core.Providers.CassandraProvider
{
    /// <summary>
    /// Cassandra Database Profile Provider
    /// </summary>
    public class CassandraProfileProvider :ProfileProvider
    {
        #region private

        private string eventSource = "CassandraProfileProvider";
        private string eventLog = "Application";
        private string exceptionMessage = "An exception occurred. Please check the Event Log.";
        private string connectionString;
        private string _applicationName;
        private static ISession _session;

        #endregion

        #region Properties

        /// <summary>Gets the session factory.</summary>
        private static ISession Session
        {
            get { return _session; }
        }

        public override string ApplicationName
        {
            get { return _applicationName; }
            set { _applicationName = value; }
        }

        public bool WriteExceptionsToEventLog { get; set; }

        #endregion

        #region Helper Functions
        // A helper function to retrieve config values from the configuration file
        private string GetConfigValue(string configValue, string defaultValue)
        {
            if (String.IsNullOrEmpty(configValue))
                return defaultValue;
            return configValue;
        }

        private void WriteToEventLog(Exception e, string action)
        {
            EventLog log = new EventLog();
            log.Source = eventSource;
            log.Log = eventLog;

            string message = exceptionMessage + "\n\n";
            message += "Action: " + action + "\n\n";
            message += "Exception: " + e.ToString();

            log.WriteEntry(message);
        }
        #endregion

        #region Private Methods
        /*
        //get a role by name
        private Model.Profiles GetProfile(string username, bool isAuthenticated)
        {
            Model.Profiles profile = null;
            //Is authenticated and IsAnonmous are opposites,so flip sign,IsAuthenticated = true -> notAnonymous
            bool isAnonymous = !isAuthenticated;

            using (ISession session = SessionFactory.OpenSession())
            {
                try
                {
                    Model.Users usr = session.CreateCriteria(typeof(Model.Users))
                                                .Add(NHibernate.Criterion.Restrictions.Eq("UserName", username))
                                                .Add(NHibernate.Criterion.Restrictions.Eq("ApplicationName", this.ApplicationName))
                                                .UniqueResult<Model.Users>();

                    if (usr != null)
                    {
                        profile = session.CreateCriteria(typeof(Model.Profiles))
                                            .Add(NHibernate.Criterion.Restrictions.Eq("UsersId", usr.Id))
                                            .Add(NHibernate.Criterion.Restrictions.Eq("IsAnonymous", isAnonymous))
                                            .UniqueResult<Model.Profiles>();
                    }
                }
                catch (Exception e)
                {
                    if (WriteExceptionsToEventLog)
                        WriteToEventLog(e, "GetProfileWithIsAuthenticated");
                    else
                        throw e;
                }

            }
            return profile;
        }

        private Model.Profiles GetProfile(string username)
        {
            Model.Profiles profile = null;
            using (ISession session = SessionFactory.OpenSession())
            {
                try
                {
                    Model.Users usr = session.CreateCriteria(typeof(Model.Users))
                                                .Add(NHibernate.Criterion.Restrictions.Eq("UserName", username))
                                                .Add(NHibernate.Criterion.Restrictions.Eq("ApplicationName", this.ApplicationName))
                                                .UniqueResult<Model.Users>();

                    if (usr != null)
                    {
                        profile = session.CreateCriteria(typeof(Model.Profiles))
                                            .Add(NHibernate.Criterion.Restrictions.Eq("UsersId", usr.Id))
                                            .UniqueResult<Model.Profiles>();
                    }
                }
                catch (Exception e)
                {
                    if (WriteExceptionsToEventLog)
                        WriteToEventLog(e, "GetProfile(username)");
                    else
                        throw e;
                }
            }
            return profile;
        }

        private Model.Profiles GetProfile(int Id)
        {
            Model.Profiles profile = null;
            using (ISession session = SessionFactory.OpenSession())
            {
                try
                {
                    Model.Users usr = session.CreateCriteria(typeof(Model.Users))
                                                .Add(NHibernate.Criterion.Restrictions.Eq("Id", Id))
                                                .Add(NHibernate.Criterion.Restrictions.Eq("ApplicationName", this.ApplicationName))
                                                .UniqueResult<Model.Users>();

                    if (usr != null)
                    {
                        profile = session.CreateCriteria(typeof(Model.Profiles))
                                            .Add(NHibernate.Criterion.Restrictions.Eq("UsersId", usr.Id))
                                            .UniqueResult<Model.Profiles>();
                    }
                    else
                        throw new ProviderException("Membership User does not exist");

                }
                catch (Exception e)
                {
                    if (WriteExceptionsToEventLog)
                        WriteToEventLog(e, "GetProfile(id)");
                    else
                        throw e;
                }

            }
            return profile;
        }

        private Model.Profiles CreateProfile(string username, bool isAnonymous)
        {
            Model.Profiles p = new Model.Profiles();
            bool profileCreated = false;

            using (ISession session = SessionFactory.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        Model.Users usr = session.CreateCriteria(typeof(Model.Users))
                                                    .Add(NHibernate.Criterion.Restrictions.Eq("UserName", username))
                                                    .Add(NHibernate.Criterion.Restrictions.Eq("ApplicationName", this.ApplicationName))
                                                    .UniqueResult<Model.Users>();

                        if (usr != null) //membership user exits so create a profile
                        {
                            p.UsersId = usr.Id;
                            p.IsAnonymous = isAnonymous;
                            p.LastUpdatedDate = System.DateTime.Now;
                            p.LastActivityDate = System.DateTime.Now;
                            p.ApplicationName = this.ApplicationName;
                            session.Save(p);
                            transaction.Commit();
                            profileCreated = true;
                        }
                        else
                            throw new ProviderException("Membership User does not exist.Profile cannot be created.");

                    }
                    catch (Exception e)
                    {
                        if (WriteExceptionsToEventLog)
                            WriteToEventLog(e, "GetProfile");
                        else
                            throw e;
                    }
                }
            }

            if (profileCreated)
                return p;
            else
                return null;

        }

        private bool IsMembershipUser(string username)
        {

            bool hasMembership = false;

            using (ISession session = SessionFactory.OpenSession())
            {

                try
                {
                    Model.Users usr = session.CreateCriteria(typeof(Model.Users))
                                                .Add(NHibernate.Criterion.Restrictions.Eq("UserName", username))
                                                .Add(NHibernate.Criterion.Restrictions.Eq("ApplicationName", this.ApplicationName))
                                                .UniqueResult<Model.Users>();

                    if (usr != null) //membership user exits so create a profile
                        hasMembership = true;
                }
                catch (Exception e)
                {
                    if (WriteExceptionsToEventLog)
                        WriteToEventLog(e, "GetProfile");
                    else
                        throw e;
                }

            }

            return hasMembership;

        }

        private bool IsUserInCollection(MembershipUserCollection uc, string username)
        {
            bool isInColl = false;
            foreach (MembershipUser u in uc)
            {
                if (u.UserName.Equals(username))
                    isInColl = true;
            }

            return isInColl;

        }

        // Updates the LastActivityDate and LastUpdatedDate values  when profile properties are accessed by the
        // GetPropertyValues and SetPropertyValues methods. Passing true as the activityOnly parameter will update only the LastActivityDate.
        private void UpdateActivityDates(string username, bool isAuthenticated, bool activityOnly)
        {
            //Is authenticated and IsAnonmous are opposites,so flip sign,IsAuthenticated = true -> notAnonymous
            bool isAnonymous = !isAuthenticated;
            DateTime activityDate = DateTime.Now;

            using (ISession session = SessionFactory.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    Model.Profiles pr = GetProfile(username, isAuthenticated);
                    if (pr == null)
                        throw new ProviderException("User Profile not found");
                    try
                    {
                        if (activityOnly)
                        {
                            pr.LastActivityDate = activityDate;
                            pr.IsAnonymous = isAnonymous;
                        }
                        else
                        {
                            pr.LastActivityDate = activityDate;
                            pr.LastUpdatedDate = activityDate;
                            pr.IsAnonymous = isAnonymous;
                        }

                        session.Update(pr);
                        transaction.Commit();
                    }
                    catch (Exception e)
                    {
                        if (WriteExceptionsToEventLog)
                        {
                            WriteToEventLog(e, "UpdateActivityDates");
                            throw new ProviderException(exceptionMessage);
                        }
                        else
                            throw e;
                    }
                }
            }
        }

        private bool DeleteProfile(string username)
        {
            // Check for valid user name.
            if (username == null)
                throw new ArgumentNullException("User name cannot be null.");
            if (username.Contains(","))
                throw new ArgumentException("User name cannot contain a comma (,).");

            Model.Profiles profile = GetProfile(username);
            if (profile == null)
                return false;

            using (ISession session = SessionFactory.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Delete(profile);
                        transaction.Commit();
                    }
                    catch (Exception e)
                    {
                        if (WriteExceptionsToEventLog)
                        {
                            WriteToEventLog(e, "DeleteProfile");
                            throw new ProviderException(exceptionMessage);
                        }
                        else
                            throw e;
                    }
                }
            }

            return true;
        }

        private bool DeleteProfile(int id)
        {
            // Check for valid user name.
            Model.Profiles profile = GetProfile(id);
            if (profile == null)
                return false;

            using (ISession session = SessionFactory.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        session.Delete(profile);
                        transaction.Commit();
                    }
                    catch (Exception e)
                    {
                        if (WriteExceptionsToEventLog)
                        {
                            WriteToEventLog(e, "DeleteProfile(id)");
                            throw new ProviderException(exceptionMessage);
                        }
                        else
                            throw e;
                    }
                }
            }

            return true;
        }

        private int DeleteProfilesbyId(string[] ids)
        {
            int deleteCount = 0;
            try
            {
                foreach (string id in ids)
                {
                    if (DeleteProfile(id))
                        deleteCount++;
                }
            }
            catch (Exception e)
            {
                if (WriteExceptionsToEventLog)
                {
                    WriteToEventLog(e, "DeleteProfiles(Id())");
                    throw new ProviderException(exceptionMessage);
                }
                else
                    throw e;

            }
            return deleteCount;
        }

        private void CheckParameters(int pageIndex, int pageSize)
        {
            if (pageIndex < 0)
                throw new ArgumentException("Page index must 0 or greater.");
            if (pageSize < 1)
                throw new ArgumentException("Page size must be greater than 0.");
        }

        private ProfileInfo GetProfileInfoFromProfile(Model.Profiles p)
        {

            Model.Users usr = null;
            using (ISession session = SessionFactory.OpenSession())
            {
                usr = session.CreateCriteria(typeof(Model.Users))
                                        .Add(NHibernate.Criterion.Restrictions.Eq("Id", p.UsersId))
                                        .Add(NHibernate.Criterion.Restrictions.Eq("ApplicationName", this.ApplicationName))
                                        .UniqueResult<Model.Users>();
            }

            if (usr == null)
                throw new ProviderException("The userid not found in memebership tables.GetProfileInfoFromProfile(p)");



            // ProfileInfo.Size not currently implemented.
            ProfileInfo pi = new ProfileInfo(usr.UserName,
                p.IsAnonymous, p.LastActivityDate, p.LastUpdatedDate, 0);

            return pi;
        }
        */
        #endregion

        #region Public Methods

        public override void Initialize(string name, NameValueCollection config)
        {
            // Initialize values from web.config.

            if (config == null)
                throw new ArgumentNullException("config");

            if (name == null || name.Length == 0)
                name = "FluentNhibernateProfileProvider";

            if (String.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "Sample Fluent Nhibernate Profile provider");
            }

            // Initialize the abstract base class.
            base.Initialize(name, config);

            _applicationName = GetConfigValue(config["applicationName"], System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
            WriteExceptionsToEventLog = Convert.ToBoolean(GetConfigValue(config["writeExceptionsToEventLog"], "true"));

            // Initialize Connection.
            ConnectionStringSettings ConnectionStringSettings = ConfigurationManager.ConnectionStrings[config["connectionStringName"]];
            if (ConnectionStringSettings == null || ConnectionStringSettings.ConnectionString.Trim() == "")
                throw new ProviderException("Connection string cannot be blank.");

            connectionString = ConnectionStringSettings.ConnectionString;
            // create our Fluent NHibernate session factory
            _session = CassandraSessionHelper.CreateSession();
        }

        public override SettingsPropertyValueCollection GetPropertyValues(SettingsContext context, SettingsPropertyCollection ppc)
        {
            /*
            string username = (string)context["UserName"];
            bool isAuthenticated = (bool)context["IsAuthenticated"];
            Model.Profiles profile = null;

            profile = GetProfile(username, isAuthenticated);
            // The serializeAs attribute is ignored in this provider implementation.
            SettingsPropertyValueCollection svc = new SettingsPropertyValueCollection();

            if (profile == null)
            {
                if (IsMembershipUser(username))
                    profile = CreateProfile(username, false);
                else
                    throw new ProviderException("Profile cannot be created. There is no membership user");
            }


            foreach (SettingsProperty prop in ppc)
            {
                SettingsPropertyValue pv = new SettingsPropertyValue(prop);
                switch (prop.Name)
                {
                    case "IsAnonymous":
                        pv.PropertyValue = profile.IsAnonymous;
                        break;
                    case "LastActivityDate":
                        pv.PropertyValue = profile.LastActivityDate;
                        break;
                    case "LastUpdatedDate":
                        pv.PropertyValue = profile.LastUpdatedDate;
                        break;
                    case "Subscription":
                        pv.PropertyValue = profile.Subscription;
                        break;
                    case "Language":
                        pv.PropertyValue = profile.Language;
                        break;
                    case "FirstName":
                        pv.PropertyValue = profile.FirstName;
                        break;
                    case "LastName":
                        pv.PropertyValue = profile.LastName;
                        break;
                    case "Gender":
                        pv.PropertyValue = profile.Gender;
                        break;
                    case "BirthDate":
                        pv.PropertyValue = profile.BirthDate;
                        break;
                    case "Occupation":
                        pv.PropertyValue = profile.Occupation;
                        break;
                    case "Website":
                        pv.PropertyValue = profile.Website;
                        break;
                    case "Street":
                        pv.PropertyValue = profile.Street;
                        break;
                    case "City":
                        pv.PropertyValue = profile.City;
                        break;
                    case "State":
                        pv.PropertyValue = profile.State;
                        break;
                    case "Zip":
                        pv.PropertyValue = profile.Zip;
                        break;
                    case "Country":
                        pv.PropertyValue = profile.Country;
                        break;

                    default:
                        throw new ProviderException("Unsupported property.");
                }

                svc.Add(pv);
            }

            UpdateActivityDates(username, isAuthenticated, true);
            return svc;
            */
            return null;
        }

        public override void SetPropertyValues(SettingsContext context, SettingsPropertyValueCollection ppvc)
        {
            /*
            Model.Profiles profile = null;
            // The serializeAs attribute is ignored in this provider implementation.
            string username = (string)context["UserName"];
            bool isAuthenticated = (bool)context["IsAuthenticated"];

            using (ISession session = SessionFactory.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {

                    profile = GetProfile(username, isAuthenticated);

                    if (profile == null)
                        profile = CreateProfile(username, !isAuthenticated);

                    foreach (SettingsPropertyValue pv in ppvc)
                    {
                        switch (pv.Property.Name)
                        {
                            case "IsAnonymous":
                                profile.IsAnonymous = (bool)pv.PropertyValue;
                                break;
                            case "LastActivityDate":
                                profile.LastActivityDate = (DateTime)pv.PropertyValue;
                                break;
                            case "LastUpdatedDate":
                                profile.LastUpdatedDate = (DateTime)pv.PropertyValue;
                                break;
                            case "Subscription":
                                profile.Subscription = pv.PropertyValue.ToString();
                                break;
                            case "Language":
                                profile.Language = pv.PropertyValue.ToString();
                                break;
                            case "FirstName":
                                profile.FirstName = pv.PropertyValue.ToString();
                                break;
                            case "LastName":
                                profile.LastName = pv.PropertyValue.ToString();
                                break;
                            case "Gender":
                                profile.Gender = pv.PropertyValue.ToString();
                                break;
                            case "BirthDate":
                                profile.BirthDate = (DateTime)pv.PropertyValue;
                                break;
                            case "Occupation":
                                profile.Occupation = pv.PropertyValue.ToString();
                                break;
                            case "Website":
                                profile.Website = pv.PropertyValue.ToString();
                                break;
                            case "Street":
                                profile.Street = pv.PropertyValue.ToString();
                                break;
                            case "City":
                                profile.City = pv.PropertyValue.ToString();
                                break;
                            case "State":
                                profile.State = pv.PropertyValue.ToString();
                                break;
                            case "Zip":
                                profile.Zip = pv.PropertyValue.ToString();
                                break;
                            case "Country":
                                profile.Country = pv.PropertyValue.ToString();
                                break;
                            default:
                                throw new ProviderException("Unsupported property.");
                        }
                    }

                    session.SaveOrUpdate(profile);
                    transaction.Commit();
                }
            }

            UpdateActivityDates(username, isAuthenticated, false);
            */
        }

        public override int DeleteProfiles(ProfileInfoCollection profiles)
        {
            /*
            int deleteCount = 0;
            try
            {
                foreach (ProfileInfo p in profiles)
                {
                    if (DeleteProfile(p.UserName))
                        deleteCount++;
                }
            }
            catch (Exception e)
            {
                if (WriteExceptionsToEventLog)
                {
                    WriteToEventLog(e, "DeleteProfiles(ProfileInfoCollection)");
                    throw new ProviderException(exceptionMessage);
                }
                else
                    throw e;
            }
            return deleteCount;
             */
            return 0;
        }

        public override int DeleteProfiles(string[] usernames)
        {
            int deleteCount = 0;
            /*
            try
            {
                foreach (string user in usernames)
                {
                    if (DeleteProfile(user))
                        deleteCount++;
                }
            }
            catch (Exception e)
            {
                if (WriteExceptionsToEventLog)
                {
                    WriteToEventLog(e, "DeleteProfiles(String())");
                    throw new ProviderException(exceptionMessage);
                }
                else
                    throw e;

            }
            */

            return deleteCount;
        }

        public override int DeleteInactiveProfiles(ProfileAuthenticationOption authenticationOption, DateTime userInactiveSinceDate)
        {
            /*
            string userIds = "";
            bool anaon = false;
            switch (authenticationOption)
            {
                case ProfileAuthenticationOption.Anonymous:
                    anaon = true;
                    break;
                case ProfileAuthenticationOption.Authenticated:
                    anaon = false;
                    break;
                default:
                    break;
            }

            using (ISession session = SessionFactory.OpenSession())
            {
                try
                {
                    IList<Model.Profiles> profs = session.CreateCriteria(typeof(Model.Profiles))
                                                    .Add(NHibernate.Criterion.Restrictions.Eq("ApplicationName", this.ApplicationName))
                                                    .Add(NHibernate.Criterion.Restrictions.Le("LastActivityDate", userInactiveSinceDate))
                                                    .Add(NHibernate.Criterion.Restrictions.Eq("IsAnonymous", anaon))
                                                    .List<Model.Profiles>();

                    if (profs != null)
                    {
                        foreach (Model.Profiles p in profs)
                            userIds += p.Id.ToString() + ",";

                    }
                }
                catch (Exception e)
                {
                    if (WriteExceptionsToEventLog)
                        WriteToEventLog(e, "DeleteInactiveProfiles");
                    else
                        throw e;
                }

            }

            if (userIds.Length > 0)
                userIds = userIds.Substring(0, userIds.Length - 1);


            return DeleteProfilesbyId(userIds.Split(','));
            */
            return 0;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="authenticationOption"></param>
        /// <param name="usernameToMatch"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <returns></returns>
        public override ProfileInfoCollection FindProfilesByUserName(ProfileAuthenticationOption authenticationOption, string usernameToMatch,
                                                                       int pageIndex,
                                                                       int pageSize,
                                                                       out int totalRecords)
        {
            /*
            CheckParameters(pageIndex, pageSize);

            return GetProfileInfo(authenticationOption, usernameToMatch, null, pageIndex, pageSize, out totalRecords);
            */
            totalRecords = 0;
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="authenticationOption"></param>
        /// <param name="usernameToMatch"></param>
        /// <param name="userInactiveSinceDate"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <returns></returns>
        public override ProfileInfoCollection FindInactiveProfilesByUserName(ProfileAuthenticationOption authenticationOption, string usernameToMatch,
                                                                              DateTime userInactiveSinceDate,
                                                                              int pageIndex,
                                                                              int pageSize,
                                                                              out int totalRecords)
        {
            /*
             CheckParameters(pageIndex, pageSize);

            return GetProfileInfo(authenticationOption, usernameToMatch, userInactiveSinceDate, pageIndex, pageSize, out totalRecords);
            */
            totalRecords = 0;
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="authenticationOption"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <returns></returns>
        public override ProfileInfoCollection GetAllProfiles(ProfileAuthenticationOption authenticationOption, int pageIndex,
                                                                              int pageSize,
                                                                              out int totalRecords)
        {
            /*
            CheckParameters(pageIndex, pageSize);

            return GetProfileInfo(authenticationOption, null, null, pageIndex, pageSize, out totalRecords);
            */
            totalRecords = 0;
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="authenticationOption"></param>
        /// <param name="userInactiveSinceDate"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <returns></returns>
        public override ProfileInfoCollection GetAllInactiveProfiles(ProfileAuthenticationOption authenticationOption, DateTime userInactiveSinceDate,
                                                                          int pageIndex,
                                                                          int pageSize,
                                                                          out int totalRecords)
        {
            /*
            CheckParameters(pageIndex, pageSize);

            return GetProfileInfo(authenticationOption, null, userInactiveSinceDate, pageIndex, pageSize, out totalRecords);
            */
            totalRecords = 0;
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="authenticationOption"></param>
        /// <param name="userInactiveSinceDate"></param>
        /// <returns></returns>
        public override int GetNumberOfInactiveProfiles(ProfileAuthenticationOption authenticationOption, DateTime userInactiveSinceDate)
        {
            int inactiveProfiles = 0;

            ProfileInfoCollection profiles =
              GetProfileInfo(authenticationOption, null, userInactiveSinceDate, 0, 0, out inactiveProfiles);

            return inactiveProfiles;
        }


        // GetProfileInfo
        // Retrieves a count of profiles and creates a 
        // ProfileInfoCollection from the profile data in the 
        // database. Called by GetAllProfiles, GetAllInactiveProfiles,
        // FindProfilesByUserName, FindInactiveProfilesByUserName, 
        // and GetNumberOfInactiveProfiles.
        // Specifying a pageIndex of 0 retrieves a count of the results only.
        //
        private ProfileInfoCollection GetProfileInfo(ProfileAuthenticationOption authenticationOption,
                                                     string usernameToMatch,
                                                     object userInactiveSinceDate,
                                                     int pageIndex,
                                                     int pageSize,
                                                     out int totalRecords)
        {

            bool isAnaon = false;
            ProfileInfoCollection profilesInfoColl = new ProfileInfoCollection();
            switch (authenticationOption)
            {
                case ProfileAuthenticationOption.Anonymous:
                    isAnaon = true;
                    break;
                case ProfileAuthenticationOption.Authenticated:
                    isAnaon = false;
                    break;
                default:
                    break;
            }

            /*

            using (ISession session = SessionFactory.OpenSession())
            {
                try
                {
                    ICriteria cprofiles = session.CreateCriteria(typeof(Model.Profiles));
                    cprofiles.Add(NHibernate.Criterion.Restrictions.Eq("ApplicationName", this.ApplicationName));



                    if (userInactiveSinceDate != null)
                        cprofiles.Add(NHibernate.Criterion.Restrictions.Le("LastActivityDate", (DateTime)userInactiveSinceDate));

                    cprofiles.Add(NHibernate.Criterion.Restrictions.Eq("IsAnonymous", isAnaon));


                    IList<Model.Profiles> profiles = cprofiles.List<Model.Profiles>();
                    IList<Model.Profiles> profiles2 = null;

                    if (profiles == null)
                        totalRecords = 0;
                    else if (profiles.Count < 1)
                        totalRecords = 0;
                    else
                        totalRecords = profiles.Count;



                    //IF USER NAME TO MATCH then fileter out those                        
                    //us.g
                    MembershipUserCollection uc = System.Web.Security.Membership.FindUsersByName(usernameToMatch);

                    if (usernameToMatch != null)
                    {
                        if (totalRecords > 0)
                        {
                            foreach (Model.Profiles p in profiles)
                            {
                                if (IsUserInCollection(uc, usernameToMatch))
                                    profiles2.Add(p);
                            }

                            if (profiles2 == null)
                                profiles2 = profiles;
                            else if (profiles2.Count < 1)
                                profiles2 = profiles;
                            else
                                totalRecords = profiles2.Count;
                        }
                        else
                            profiles2 = profiles;
                    }
                    else
                        profiles2 = profiles;


                    if (totalRecords <= 0)
                        return profilesInfoColl;

                    if (pageSize == 0)
                        return profilesInfoColl;

                    int counter = 0;
                    int startIndex = pageSize * (pageIndex - 1);
                    int endIndex = startIndex + pageSize - 1;

                    foreach (Model.Profiles p in profiles2)
                    {
                        if (counter >= endIndex)
                            break;
                        if (counter >= startIndex)
                        {
                            ProfileInfo pi = GetProfileInfoFromProfile(p);
                            profilesInfoColl.Add(pi);
                        }
                        counter++;
                    }
                }
                catch (Exception e)
                {
                    if (WriteExceptionsToEventLog)
                    {
                        WriteToEventLog(e, "GetProfileInfo");
                        throw new ProviderException(exceptionMessage);
                    }
                    else
                        throw e;

                }
            }
            */
            totalRecords = 0;
            return profilesInfoColl;
        }

        #endregion
    }
}
