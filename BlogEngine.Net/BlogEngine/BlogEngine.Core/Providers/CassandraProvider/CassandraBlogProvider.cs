﻿#region Using

using System;
using System.Collections.Generic;
using System.Collections.Specialized;

using BlogEngine.Core.DataStore;
using BlogEngine.Core.Notes;
using BlogEngine.Core.Packaging;

#endregion

namespace BlogEngine.Core.Providers
{
    /// <summary>
    /// Cassandra Database BlogProvider 
    /// </summary>
    public class CassandraBlogProvider :BlogProvider
    {
        // Post
        #region Public Methods

        /// <summary>
        /// Deletes a BlogRoll from the data store specified by the provider.
        /// </summary>
        /// <param name="blogRollItem">
        /// The blog Roll Item to delete.
        /// </param>
        public override void DeleteBlogRollItem(BlogRollItem blogRollItem)
        {
            //TODO
        }

        /// <summary>
        /// Deletes a Blog from the data store specified by the provider.
        /// </summary>
        /// <param name="blog">
        /// The blog to delete.
        /// </param>
        public override void DeleteBlog(Blog blog)
        {
            //TODO
        }

        /// <summary>
        /// Deletes a Blog's storage container from the data store specified by the provider.
        /// </summary>
        /// <param name="blog">
        /// The blog to delete the storage container of.
        /// </param>
        public override bool DeleteBlogStorageContainer(Blog blog)
        {
            //TODO
            return false;
        }

        /// <summary>
        /// Deletes a Category from the data store specified by the provider.
        /// </summary>
        /// <param name="category">
        /// The category to delete.
        /// </param>
        public override void DeleteCategory(Category category)
        {
            //TODO
        }

        /// <summary>
        /// Deletes a Page from the data store specified by the provider.
        /// </summary>
        /// <param name="page">
        /// The page to delete.
        /// </param>
        public override void DeletePage(Page page)
        {
            //TODO
        }

        /// <summary>
        /// Deletes a Post from the data store specified by the provider.
        /// </summary>
        /// <param name="post">
        /// The post to delete.
        /// </param>
        public override void DeletePost(Post post)
        {
            //TODO
        }

        /// <summary>
        /// Deletes a Page from the data store specified by the provider.
        /// </summary>
        /// <param name="profile">
        /// The profile to delete.
        /// </param>
        public override void DeleteProfile(AuthorProfile profile)
        {
            //TODO
        }

        /// <summary>
        /// Retrieves all BlogRolls from the provider and returns them in a list.
        /// </summary>
        /// <returns>A list of BlogRollItem.</returns>
        public override List<BlogRollItem> FillBlogRoll()
        {
            //TODO
            return null;
        }

        /// <summary>
        /// Retrieves all Blogs from the provider and returns them in a list.
        /// </summary>
        /// <returns>A list of Blogs.</returns>
        public override List<Blog> FillBlogs()
        {
            //TODO
            return null;
        }

        /// <summary>
        /// Retrieves all Categories from the provider and returns them in a List.
        /// </summary>
        /// <returns>A list of Category.</returns>
        public override List<Category> FillCategories(Blog blog)
        {
            //TODO
            return null;
        }

        /// <summary>
        /// Retrieves all Pages from the provider and returns them in a List.
        /// </summary>
        /// <returns>A list of Page.</returns>
        public override List<Page> FillPages()
        {
            //TODO
            return null;
        }

        /// <summary>
        /// Retrieves all Posts from the provider and returns them in a List.
        /// </summary>
        /// <returns>A list of Post.</returns>
        public override List<Post> FillPosts()
        {
            //TODO
            return null;
        }

        /// <summary>
        /// Retrieves all Pages from the provider and returns them in a List.
        /// </summary>
        /// <returns>A list of AuthorProfile.</returns>
        public override List<AuthorProfile> FillProfiles()
        {
            //TODO
            return null;
        }

        /// <summary>
        /// Deletes a Referrer from the data store specified by the provider.
        /// </summary>
        /// <returns>A list of Referrer.</returns>
        public override List<Referrer> FillReferrers()
        {
            //TODO
            return null;
        }

        /// <summary>
        /// Returns a dictionary representing rights and the roles that allow them.
        /// </summary>
        /// <returns>
        /// 
        /// The key must be a string of the name of the Rights enum of the represented Right.
        /// The value must be an IEnumerable of strings that includes only the role names of
        /// roles the right represents.
        /// 
        /// Inheritors do not need to worry about verifying that the keys and values are valid.
        /// This is handled in the Right class.
        /// 
        /// </returns>
        public override IDictionary<string, IEnumerable<String>> FillRights()
        {
            //TODO
            return null;
        }

        /// <summary>
        /// Inserts a new BlogRoll into the data store specified by the provider.
        /// </summary>
        /// <param name="blogRollItem">
        /// The blog Roll Item.
        /// </param>
        public override void InsertBlogRollItem(BlogRollItem blogRollItem)
        {
            //TODO           
        }

        /// <summary>
        /// Inserts a new Blog into the data store specified by the provider.
        /// </summary>
        /// <param name="blog">
        /// The blog.
        /// </param>
        public override void InsertBlog(Blog blog)
        {
            //TODO

        }

        /// <summary>
        /// Inserts a new Category into the data store specified by the provider.
        /// </summary>
        /// <param name="category">
        /// The category.
        /// </param>
        public override void InsertCategory(Category category)
        {
            //TODO

        }

        /// <summary>
        /// Inserts a new Page into the data store specified by the provider.
        /// </summary>
        /// <param name="page">
        /// The page to insert.
        /// </param>
        public override void InsertPage(Page page)
        {
            //TODO

        }

        /// <summary>
        /// Inserts a new Post into the data store specified by the provider.
        /// </summary>
        /// <param name="post">
        /// The post to insert.
        /// </param>
        public override void InsertPost(Post post)
        {
            //TODO
        }

        /// <summary>
        /// Inserts a new Page into the data store specified by the provider.
        /// </summary>
        /// <param name="profile">
        /// The profile to insert.
        /// </param>
        public override void InsertProfile(AuthorProfile profile)
        {
            //TODO

        }

        /// <summary>
        /// Inserts a new Referrer into the data store specified by the provider.
        /// </summary>
        /// <param name="referrer">
        /// The referrer to insert.
        /// </param>
        public override void InsertReferrer(Referrer referrer)
        {
            //TODO          
        }

        /// <summary>
        /// Loads settings from data store
        /// </summary>
        /// <param name="extensionType">
        /// Extension Type
        /// </param>
        /// <param name="extensionId">
        /// Extensio Id
        /// </param>
        /// <returns>
        /// Settings as stream
        /// </returns>
        public override object LoadFromDataStore(ExtensionType extensionType, string extensionId)
        {
            //TODO
            return null;
        }

        /// <summary>
        /// Loads the ping services.
        /// </summary>
        /// <returns>
        /// A StringCollection.
        /// </returns>
        public override StringCollection LoadPingServices()
        {
            //TODO
            return null;
        }

        /// <summary>
        /// Loads the settings from the provider.
        /// </summary>
        /// <returns>A StringDictionary.</returns>
        public override StringDictionary LoadSettings(Blog blog)
        {
            //TODO
            return null;
        }

        /// <summary>
        /// Loads the stop words used in the search feature.
        /// </summary>
        /// <returns>
        /// A StringCollection.
        /// </returns>
        public override StringCollection LoadStopWords()
        {
            //TODO
            return null;
        }

        /// <summary>
        /// Removes settings from data store
        /// </summary>
        /// <param name="extensionType">
        /// Extension Type
        /// </param>
        /// <param name="extensionId">
        /// Extension Id
        /// </param>
        public override void RemoveFromDataStore(ExtensionType extensionType, string extensionId)
        {

        }

        /// <summary>
        /// Saves the ping services.
        /// </summary>
        /// <param name="services">
        /// The services.
        /// </param>
        public override void SavePingServices(StringCollection services)
        {

        }

        /// <summary>
        /// Saves all of the Rights and the roles that coorespond with them.
        /// </summary>
        /// <param name="rights"></param>
        public override void SaveRights(IEnumerable<Right> rights)
        {

        }

        /// <summary>
        /// Saves the settings to the provider.
        /// </summary>
        /// <param name="settings">
        /// The settings.
        /// </param>
        public override void SaveSettings(StringDictionary settings)
        {

        }

        /// <summary>
        /// Saves settings to data store
        /// </summary>
        /// <param name="extensionType">
        /// Extension Type
        /// </param>
        /// <param name="extensionId">
        /// Extension Id
        /// </param>
        /// <param name="settings">
        /// Settings object
        /// </param>
        public override void SaveToDataStore(ExtensionType extensionType, string extensionId, object settings)
        {

        }

        /// <summary>
        /// Retrieves a BlogRoll from the provider based on the specified id.
        /// </summary>
        /// <param name="id">The Blog Roll Item Id.</param>
        /// <returns>A BlogRollItem.</returns>
        public override BlogRollItem SelectBlogRollItem(Guid id)
        {
            //TODO
            return null;
        }

        /// <summary>
        /// Retrieves a Blog from the provider based on the specified id.
        /// </summary>
        /// <param name="id">The Blog Id.</param>
        /// <returns>A Blog.</returns>
        public override Blog SelectBlog(Guid id)
        {
            //TODO
            return null;
        }

        /// <summary>
        /// Retrieves a Category from the provider based on the specified id.
        /// </summary>
        /// <param name="id">The Category id.</param>
        /// <returns>A Category.</returns>
        public override Category SelectCategory(Guid id)
        {
            //TODO
            return null;
        }

        /// <summary>
        /// Retrieves a Page from the provider based on the specified id.
        /// </summary>
        /// <param name="id">The Page id.</param>
        /// <returns>The Page object.</returns>
        public override Page SelectPage(Guid id)
        {
            //TODO
            return null;
        }

        /// <summary>
        /// Retrieves a Post from the provider based on the specified id.
        /// </summary>
        /// <param name="id">The Post id.</param>
        /// <returns>A Post object.</returns>
        public override Post SelectPost(Guid id)
        {
            //TODO
            return null;
        }

        /// <summary>
        /// Retrieves a Page from the provider based on the specified id.
        /// </summary>
        /// <param name="id">The AuthorProfile id.</param>
        /// <returns>An AuthorProfile.</returns>
        public override AuthorProfile SelectProfile(string id)
        {
            //TODO
            return null;
        }

        /// <summary>
        /// Retrieves a Referrer from the provider based on the specified id.
        /// </summary>
        /// <param name="id">The Referrer Id.</param>
        /// <returns>A Referrer.</returns>
        public override Referrer SelectReferrer(Guid id)
        {
            //TODO
            return null;
        }

        /// <summary>
        /// Sets up the required storage files/tables for a new Blog instance, from an existing blog instance.
        /// </summary>
        /// <param name="existingBlog">The existing blog instance to base the new blog instance off of.</param>
        /// <param name="newBlog">The new blog instance.</param>
        /// <returns>A boolean indicating if the setup process was successful.</returns>
        public override bool SetupBlogFromExistingBlog(Blog existingBlog, Blog newBlog)
        {
            //TODO
            return false;
        }

        /// <summary>
        /// Setup new blog
        /// </summary>
        /// <param name="newBlog">New blog</param>
        /// <param name="userName">User name</param>
        /// <param name="email">Email</param>
        /// <param name="password">Password</param>
        /// <returns>True if successful</returns>
        public override bool SetupNewBlog(Blog newBlog, string userName, string email, string password)
        {
            //TODO
            return false;
        }

        /// <summary>
        /// Updates an existing BlogRollItem in the data store specified by the provider.
        /// </summary>
        /// <param name="blogRollItem">
        /// The blogroll item to update.
        /// </param>
        public override void UpdateBlogRollItem(BlogRollItem blogRollItem)
        {

        }

        /// <summary>
        /// Updates an existing Blog in the data store specified by the provider.
        /// </summary>
        /// <param name="blog">
        /// The blog to update.
        /// </param>
        public override void UpdateBlog(Blog blog)
        {

        }

        /// <summary>
        /// Updates an existing Category in the data store specified by the provider.
        /// </summary>
        /// <param name="category">
        /// The category to update.
        /// </param>
        public override void UpdateCategory(Category category)
        {

        }

        /// <summary>
        /// Updates an existing Page in the data store specified by the provider.
        /// </summary>
        /// <param name="page">
        /// The page to update.
        /// </param>
        public override void UpdatePage(Page page)
        {

        }

        /// <summary>
        /// Updates an existing Post in the data store specified by the provider.
        /// </summary>
        /// <param name="post">
        /// The post to update.
        /// </param>
        public override void UpdatePost(Post post)
        {

        }

        /// <summary>
        /// Updates an existing Page in the data store specified by the provider.
        /// </summary>
        /// <param name="profile">
        /// The profile to update.
        /// </param>
        public override void UpdateProfile(AuthorProfile profile)
        {

        }

        /// <summary>
        /// Updates an existing Referrer in the data store specified by the provider.
        /// </summary>
        /// <param name="referrer">
        /// The referrer to update.
        /// </param>
        public override void UpdateReferrer(Referrer referrer)
        {

        }

        #region Packaging

        /// <summary>
        /// Save installed package id and version
        /// </summary>
        /// <param name="package">Intalled package</param>
        public override void SavePackage(InstalledPackage package)
        {

        }

        /// <summary>
        /// Log of all files for installed package
        /// </summary>
        /// <param name="packageFiles">List of intalled package files</param>
        public override void SavePackageFiles(List<PackageFile> packageFiles)
        {

        }

        /// <summary>
        /// Gets list of files for installed package
        /// </summary>
        /// <param name="packageId">Package ID</param>
        /// <returns>List of files for installed package</returns>
        public override List<PackageFile> FillPackageFiles(string packageId)
        {
            //TODO
            return null;
        }

        /// <summary>
        /// Gets all installed from gallery packages
        /// </summary>
        /// <returns>List of installed packages</returns>
        public override List<InstalledPackage> FillPackages()
        {
            //TODO
            return null;
        }

        /// <summary>
        /// Should delete package and remove all package files
        /// </summary>
        /// <param name="packageId">Package ID</param>
        public override void DeletePackage(string packageId)
        {
            //TODO
        }

        #endregion

        #region QuickNotes

        /// <summary>
        /// Save quick note
        /// </summary>
        /// <param name="note">Quick note</param>
        public override void SaveQuickNote(QuickNote note)
        {

        }

        /// <summary>
        /// Save quick setting
        /// </summary>
        /// <param name="setting">Quick setting</param>
        public override void SaveQuickSetting(QuickSetting setting)
        {

        }

        /// <summary>
        /// Fill quick notes
        /// </summary>
        /// <param name="userId">User ID</param>
        /// <returns>List of user notes</returns>
        public override List<QuickNote> FillQuickNotes(string userId)
        {
            //TODO
            return null;
        }

        /// <summary>
        /// Fill quick settings
        /// </summary>
        /// <param name="userId">User ID</param>
        /// <returns>List of user settings</returns>
        public override List<QuickSetting> FillQuickSettings(string userId)
        {
            //TODO
            return null;
        }

        /// <summary>
        /// Delete quick note
        /// </summary>
        /// <param name="noteId">Note ID</param>
        public override void DeleteQuickNote(Guid noteId)
        {
            //TODO
        }

        #endregion

        #region CustomFields

        /// <summary>
        /// Saves custom field
        /// </summary>
        /// <param name="field">Object custom field</param>
        public override void SaveCustomField(BlogEngine.Core.Data.Models.CustomField field)
        {

        }

        /// <summary>
        /// Fills list of custom fields for a blog
        /// </summary>
        /// <returns>List of custom fields</returns>
        public override List<BlogEngine.Core.Data.Models.CustomField> FillCustomFields()
        {
            //TODO
            return null;
        }

        /// <summary>
        /// Deletes custom field
        /// </summary>
        /// <param name="field">Object field</param>
        public override void DeleteCustomField(BlogEngine.Core.Data.Models.CustomField field)
        {
            //TODO           
        }

        #endregion

        #endregion
    }
}
