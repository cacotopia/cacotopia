﻿#region Using

using NHibernate;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Configuration.Provider;
using System.Data;
using System.Data.Odbc;
using System.Diagnostics;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Security;

#endregion

namespace BlogEngine.Core.Providers
{
    /// <summary>
    /// 
    /// </summary>
    public class FluentNhibernateMembershipProvider:MembershipProvider
    {
        #region Private

        // Global connection string, generated password length, generic exception message, event log info.
        private int newPasswordLength = 8;

        private string eventSource = "FluentNhibernateMembershipProvider";
        private string eventLog = "Application";
        private string exceptionMessage = "An exception occurred. Please check the Event Log.";
        private string connectionString;

        private bool _enablePasswordReset;
        private bool _enablePasswordRetrieval;
        private bool _requiresQuestionAndAnswer;
        private bool _requiresUniqueEmail;

        private int _maxInvalidPasswordAttempts;
        private int _passwordAttemptWindow;
        private int _minRequiredNonAlphanumericCharacters;
        private int _minRequiredPasswordLength;

        private string _passwordStrengthRegularExpression;
        private string _applicationName;
        private string _description = "FluentNhibernateMembershipProvider";
        //private string _name;

        //NHibernate SessionFactory
        private static ISessionFactory _sessionFactory;
        private MembershipPasswordFormat _passwordFormat;
        // Used when determining encryption key values.
        private MachineKeySection _machineKey;

        #endregion

        #region Public Propeties

        /// <summary>
        /// 使用自定义成员资格提供程序的应用程序的名称
        /// </summary>
        public override string ApplicationName
        {
            get { return _applicationName; }
            set { _applicationName = value; }
        }

        /// <summary>
        /// 获取一条简短的易懂描述，它适合在管理工具或其他用户界面 (UI) 中显示
        /// </summary>
        public override string Description
        {
            get
            {
                return _description;
            }
        }

        /// <summary>
        /// 指示成员资格提供程序是否配置为允许用户重置其密码
        /// </summary>
        public override bool EnablePasswordReset
        {
            get { return _enablePasswordReset; }
        }

        /// <summary>
        /// 指示成员资格提供程序是否配置为允许用户检索其密码
        /// </summary>
        public override bool EnablePasswordRetrieval
        {
            get { return _enablePasswordRetrieval; }
        }

        /// <summary>
        /// 指示成员资格提供程序是否配置为要求用户在进行密码重置和检索时回答密码提示问题
        /// </summary>
        public override bool RequiresQuestionAndAnswer
        {
            get { return _requiresQuestionAndAnswer; }
        }

        /// <summary>
        /// 获取一个值，指示成员资格提供程序是否配置为要求每个用户名具有唯一的电子邮件地址
        /// </summary>
        public override bool RequiresUniqueEmail
        {
            get { return _requiresUniqueEmail; }
        }

        /// <summary>
        /// 获取锁定成员资格用户前允许的无效密码或无效密码提示问题答案尝试次数
        /// </summary>
        public override int MaxInvalidPasswordAttempts
        {
            get { return _maxInvalidPasswordAttempts; }
        }

        /// <summary>
        /// 获取在锁定成员资格用户之前允许的最大无效密码或无效密码提示问题答案尝试次数的分钟数
        /// </summary>
        public override int PasswordAttemptWindow
        {
            get { return _passwordAttemptWindow; }
        }

        /// <summary>
        /// 获得在配置过程中引用提供程序的名称
        /// </summary>
        //public override string Name
        //{
        //    get
        //    {
        //        return _name;
        //    }
        //}

        /// <summary>
        /// 获取在成员资格数据存储区中存储密码的格式
        /// </summary>
        public override MembershipPasswordFormat PasswordFormat
        {
            get { return _passwordFormat; }
        }

        /// <summary>
        /// 获取有效密码中必须包含的最少特殊字符数
        /// </summary>
        public override int MinRequiredNonAlphanumericCharacters
        {
            get { return _minRequiredNonAlphanumericCharacters; }
        }

        /// <summary>
        /// 获取密码所要求的最小长度
        /// </summary>
        public override int MinRequiredPasswordLength
        {
            get { return _minRequiredPasswordLength; }
        }

        /// <summary>
        /// 获取用于计算密码的正则表达式
        /// </summary>
        public override string PasswordStrengthRegularExpression
        {
            get { return _passwordStrengthRegularExpression; }
        }

        // If false, exceptions are thrown to the caller. If true,
        // exceptions are written to the event log.
        /// <summary>
        /// 获取或设置是否写异常信息到Windows事件日志
        /// </summary>
        public bool WriteExceptionsToEventLog { get; set; }

        /// <summary>Gets the session factory.</summary>
        private static ISessionFactory SessionFactory
        {
            get { return _sessionFactory; }
        }

        #endregion

        #region Helper functions

        // A Function to retrieve config values from the configuration file
        private string GetConfigValue(string configValue, string defaultValue)
        {
            if (String.IsNullOrEmpty(configValue))
                return defaultValue;

            return configValue;
        }

        /// <summary>
        /// Fn to create a Membership user from a Entities.Users class
        /// </summary>
        /// <param name="usr"></param>
        /// <returns></returns>
        //private MembershipUser GetMembershipUserFromUser(Users usr)
        //{
        //    MembershipUser u = new MembershipUser(this.Name,
        //                                          usr.UserName,
        //                                          usr.Id,
        //                                          usr.Email,
        //                                          usr.PasswordQuestion,
        //                                          usr.Comment,
        //                                          usr.IsApproved,
        //                                          usr.IsLockedOut,
        //                                          usr.CreationDate,
        //                                          usr.LastLoginDate,
        //                                          usr.LastActivityDate,
        //                                          usr.LastPasswordChangedDate,
        //                                          usr.LastLockedOutDate);

        //    return u;
        //}

        /// <summary>
        /// Fn that performs the checks and updates associated with password failure tracking
        /// </summary>
        /// <param name="username"></param>
        /// <param name="failureType"></param>
        //private void UpdateFailureCount(string username, string failureType)
        //{
        //    DateTime windowStart = new DateTime();
        //    int failureCount = 0;
        //    Model.Users usr = null;

        //    using (ISession session = SessionFactory.OpenSession())
        //    {
        //        using (ITransaction transaction = session.BeginTransaction())
        //        {
        //            try
        //            {
        //                usr = GetUserByUsername(username);

        //                if (!(usr == null))
        //                {
        //                    if (failureType == "password")
        //                    {
        //                        failureCount = usr.FailedPasswordAttemptCount;
        //                        windowStart = usr.FailedPasswordAttemptWindowStart;
        //                    }

        //                    if (failureType == "passwordAnswer")
        //                    {
        //                        failureCount = usr.FailedPasswordAnswerAttemptCount;
        //                        windowStart = usr.FailedPasswordAnswerAttemptWindowStart;
        //                    }
        //                }

        //                DateTime windowEnd = windowStart.AddMinutes(PasswordAttemptWindow);

        //                if (failureCount == 0 || DateTime.Now > windowEnd)
        //                {
        //                    // First password failure or outside of PasswordAttemptWindow. 
        //                    // Start a new password failure count from 1 and a new window starting now.

        //                    if (failureType == "password")
        //                    {
        //                        usr.FailedPasswordAttemptCount = 1;
        //                        usr.FailedPasswordAttemptWindowStart = DateTime.Now; ;
        //                    }

        //                    if (failureType == "passwordAnswer")
        //                    {
        //                        usr.FailedPasswordAnswerAttemptCount = 1;
        //                        usr.FailedPasswordAnswerAttemptWindowStart = DateTime.Now; ;
        //                    }
        //                    session.Update(usr);
        //                    transaction.Commit();
        //                }
        //                else
        //                {
        //                    if (failureCount++ >= MaxInvalidPasswordAttempts)
        //                    {
        //                        // Password attempts have exceeded the failure threshold. Lock out
        //                        // the user.
        //                        usr.IsLockedOut = true;
        //                        usr.LastLockedOutDate = DateTime.Now;
        //                        session.Update(usr);
        //                        transaction.Commit();
        //                    }
        //                    else
        //                    {
        //                        // Password attempts have not exceeded the failure threshold. Update
        //                        // the failure counts. Leave the window the same.

        //                        if (failureType == "password")
        //                            usr.FailedPasswordAttemptCount = failureCount;

        //                        if (failureType == "passwordAnswer")
        //                            usr.FailedPasswordAnswerAttemptCount = failureCount;

        //                        session.Update(usr);
        //                        transaction.Commit();
        //                    }
        //                }
        //            }
        //            catch (Exception e)
        //            {
        //                if (WriteExceptionsToEventLog)
        //                {
        //                    WriteToEventLog(e, "UpdateFailureCount");
        //                    throw new ProviderException("Unable to update failure count and window start." + exceptionMessage);
        //                }
        //                else
        //                    throw e;
        //            }
        //        }
        //    }
        //}

        /// <summary>
        /// CheckPassword: Compares password values based on the MembershipPasswordFormat.
        /// </summary>
        /// <param name="password"></param>
        /// <param name="dbpassword"></param>
        /// <returns></returns>
        private bool CheckPassword(string password, string dbpassword)
        {
            string pass1 = password;
            string pass2 = dbpassword;

            switch (PasswordFormat)
            {
                case MembershipPasswordFormat.Encrypted:
                    pass2 = UnEncodePassword(dbpassword);
                    break;
                case MembershipPasswordFormat.Hashed:
                    pass1 = EncodePassword(password);
                    break;
                default:
                    break;
            }

            if (pass1 == pass2)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// EncodePassword:Encrypts, Hashes, or leaves the password clear based on the PasswordFormat.
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        private string EncodePassword(string password)
        {
            string encodedPassword = password;

            switch (PasswordFormat)
            {
                case MembershipPasswordFormat.Clear:
                    break;
                case MembershipPasswordFormat.Encrypted:
                    encodedPassword =
                      Convert.ToBase64String(EncryptPassword(Encoding.Unicode.GetBytes(password)));
                    break;
                case MembershipPasswordFormat.Hashed:
                    HMACSHA1 hash = new HMACSHA1();
                    hash.Key = HexToByte(_machineKey.ValidationKey);
                    encodedPassword =
                      Convert.ToBase64String(hash.ComputeHash(Encoding.Unicode.GetBytes(password)));
                    break;
                default:
                    throw new ProviderException("Unsupported password format.");
            }
            return encodedPassword;
        }

        /// <summary>
        /// UnEncodePassword :Decrypts or leaves the password clear based on the PasswordFormat.
        /// </summary>
        /// <param name="encodedPassword"></param>
        /// <returns></returns>
        private string UnEncodePassword(string encodedPassword)
        {
            string password = encodedPassword;

            switch (PasswordFormat)
            {
                case MembershipPasswordFormat.Clear:
                    break;
                case MembershipPasswordFormat.Encrypted:
                    password =
                      Encoding.Unicode.GetString(DecryptPassword(Convert.FromBase64String(password)));
                    break;
                case MembershipPasswordFormat.Hashed:
                    throw new ProviderException("Cannot unencode a hashed password.");
                default:
                    throw new ProviderException("Unsupported password format.");
            }

            return password;
        }

        /// <summary>
        /// Converts a hexadecimal string to a byte array. Used to convert encryption key values from the configuration.
        /// </summary>
        /// <param name="hexString"></param>
        /// <returns></returns>       
        private byte[] HexToByte(string hexString)
        {
            byte[] returnBytes = new byte[hexString.Length / 2];

            for (int i = 0; i < returnBytes.Length; i++)
            {
                returnBytes[i] = Convert.ToByte(hexString.Substring(i * 2, 2), 16);
            }

            return returnBytes;
        }

        /// <summary>
        /// WriteToEventLog
        /// A helper function that writes exception detail to the event log. Exceptions
        /// are written to the event log as a security measure to avoid private database
        /// details from being returned to the browser. If a method does not return a status
        /// or boolean indicating the action succeeded or failed, a generic exception is also 
        /// thrown by the caller.
        /// </summary>
        /// <param name="e"></param>
        /// <param name="action"></param>
        private void WriteToEventLog(Exception e, string action)
        {
            EventLog log = new EventLog();
            log.Source = eventSource;
            log.Log = eventLog;

            string message = "An exception occurred communicating with the data source.\n\n";
            message += "Action: " + action + "\n\n";
            message += "Exception: " + e.ToString();

            log.WriteEntry(message);
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// single fn to get a membership user by key or username
        /// </summary>
        /// <param name="isKeySupplied"></param>
        /// <param name="username"></param>
        /// <param name="providerUserKey"></param>
        /// <param name="userIsOnline"></param>
        /// <returns></returns>
        /*
         private MembershipUser GetMembershipUserByKeyOrUser(bool isKeySupplied, string username, object providerUserKey, bool userIsOnline)
        {
            Model.Users usr = null;
            MembershipUser u = null;

            using (ISession session = SessionFactory.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {

                    try
                    {
                        if (isKeySupplied)
                            usr = session.CreateCriteria(typeof(Model.Users))
                                            .Add(NHibernate.Criterion.Restrictions.Eq("Id", (int)providerUserKey))
                                            .UniqueResult<Model.Users>();

                        else
                            usr = session.CreateCriteria(typeof(Model.Users))
                                            .Add(NHibernate.Criterion.Restrictions.Eq("UserName", username))
                                            .Add(NHibernate.Criterion.Restrictions.Eq("ApplicationName", this.ApplicationName))
                                            .UniqueResult<Model.Users>();

                        if (usr != null)
                        {

                            if (userIsOnline)
                            {
                                usr.LastActivityDate = System.DateTime.Now;
                                session.Update(usr);
                                transaction.Commit();
                            }

                            u = GetMembershipUserFromUser(usr);
                        }
                    }
                    catch (Exception e)
                    {
                        if (WriteExceptionsToEventLog)
                            WriteToEventLog(e, "GetUser(Object, Boolean)");
                        throw new ProviderException(exceptionMessage);
                    }
                }
            }
            return u;
        }
         */


        /// <summary>
        /// 通过用户名获取用户信息
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        /*
         private Model.Users GetUserByUsername(string username)
        {
            Model.Users usr = null;
            using (ISession session = SessionFactory.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {

                    try
                    {
                        usr = session.CreateCriteria(typeof(Model.Users))
                                        .Add(NHibernate.Criterion.Restrictions.Eq("UserName", username))
                                        .Add(NHibernate.Criterion.Restrictions.Eq("ApplicationName", this.ApplicationName))
                                        .UniqueResult<Model.Users>();


                    }
                    catch (Exception e)
                    {
                        if (WriteExceptionsToEventLog)
                            WriteToEventLog(e, "UnlockUser");
                        throw new ProviderException(exceptionMessage);
                    }
                }
            }
            return usr;
        }
        */

        /// <summary>
        /// 获取所用在册用户信息
        /// </summary>
        /// <returns></returns>
        /*
         private IList<Model.Users> GetUsers()
        {
            IList<Model.Users> usrs = null;
            using (ISession session = SessionFactory.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {

                    try
                    {
                        usrs = session.CreateCriteria(typeof(Model.Users))
                                        .Add(NHibernate.Criterion.Restrictions.Eq("ApplicationName", this.ApplicationName))
                                        .List<Model.Users>();

                    }
                    catch (Exception e)
                    {
                        if (WriteExceptionsToEventLog)
                            WriteToEventLog(e, "GetUsers");
                        throw new ProviderException(exceptionMessage);
                    }
                }
            }
            return usrs;

        }
        */

        /// <summary>
        /// 通过用户名模糊查询用户信息
        /// </summary>
        /// <param name="usernameToMatch"></param>
        /// <returns></returns>
        /*
         private IList<Model.Users> GetUsersLikeUserName(string usernameToMatch)
        {
            IList<Model.Users> usrs = null;
            using (ISession session = SessionFactory.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {

                    try
                    {
                        usrs = session.CreateCriteria(typeof(Model.Users))
                                        .Add(NHibernate.Criterion.Restrictions.Like("UserName", usernameToMatch))
                                        .Add(NHibernate.Criterion.Restrictions.Eq("ApplicationName", this.ApplicationName))
                                        .List<Model.Users>();

                    }
                    catch (Exception e)
                    {
                        if (WriteExceptionsToEventLog)
                            WriteToEventLog(e, "GetUsersMatchByUserName");
                        throw new ProviderException(exceptionMessage);
                    }
                }
            }
            return usrs;
        }
        */


        /// <summary>
        /// 通过Email模糊查询用户信息
        /// </summary>
        /// <param name="emailToMatch"></param>
        /// <returns></returns>
        /*
         private IList<Model.Users> GetUsersLikeEmail(string emailToMatch)
        {
            IList<Model.Users> usrs = null;
            using (ISession session = SessionFactory.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {

                    try
                    {
                        usrs = session.CreateCriteria(typeof(Model.Users))
                                        .Add(NHibernate.Criterion.Restrictions.Like("Email", emailToMatch))
                                        .Add(NHibernate.Criterion.Restrictions.Eq("ApplicationName", this.ApplicationName))
                                        .List<Model.Users>();

                    }
                    catch (Exception e)
                    {
                        if (WriteExceptionsToEventLog)
                            WriteToEventLog(e, "GetUsersMatchByEmail");
                        throw new ProviderException(exceptionMessage);
                    }
                }
            }
            return usrs;
        }
        */

        #endregion

        #region Public methods

        // Initilaize the provider 
        /// <summary>
        /// 初始化提供程序
        /// </summary>
        /// <param name="name"></param>
        /// <param name="config"></param>
        public override void Initialize(string name, NameValueCollection config)
        {
            // Initialize values from web.config.
            if (config == null)
                throw new ArgumentNullException("config");

            if (name == null || name.Length == 0)
                name = "FluentNhibernateMemebershipProvider";

            if (String.IsNullOrEmpty(config["description"]))
            {
                config.Remove("description");
                config.Add("description", "Fluent Nhibernate Membership provider");
            }

            // Initialize the abstract base class.
            base.Initialize(name, config);

            _applicationName = GetConfigValue(config["applicationName"], System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);

            _maxInvalidPasswordAttempts = Convert.ToInt32(GetConfigValue(config["maxInvalidPasswordAttempts"], "5"));
            _passwordAttemptWindow = Convert.ToInt32(GetConfigValue(config["passwordAttemptWindow"], "10"));
            _minRequiredNonAlphanumericCharacters = Convert.ToInt32(GetConfigValue(config["minRequiredNonAlphanumericCharacters"], "1"));
            _minRequiredPasswordLength = Convert.ToInt32(GetConfigValue(config["minRequiredPasswordLength"], "7"));

            _passwordStrengthRegularExpression = Convert.ToString(GetConfigValue(config["passwordStrengthRegularExpression"], ""));

            _enablePasswordReset = Convert.ToBoolean(GetConfigValue(config["enablePasswordReset"], "true"));
            _enablePasswordRetrieval = Convert.ToBoolean(GetConfigValue(config["enablePasswordRetrieval"], "true"));
            _requiresQuestionAndAnswer = Convert.ToBoolean(GetConfigValue(config["requiresQuestionAndAnswer"], "false"));
            _requiresUniqueEmail = Convert.ToBoolean(GetConfigValue(config["requiresUniqueEmail"], "true"));
            WriteExceptionsToEventLog = Convert.ToBoolean(GetConfigValue(config["writeExceptionsToEventLog"], "true"));

            string temp_format = config["passwordFormat"];
            if (temp_format == null)
            {
                temp_format = "Hashed";
            }

            switch (temp_format)
            {
                case "Hashed":
                    _passwordFormat = MembershipPasswordFormat.Hashed;
                    break;
                case "Encrypted":
                    _passwordFormat = MembershipPasswordFormat.Encrypted;
                    break;
                case "Clear":
                    _passwordFormat = MembershipPasswordFormat.Clear;
                    break;
                default:
                    throw new ProviderException("Password format not supported.");
            }


            //
            // Initialize Connection.
            ConnectionStringSettings ConnectionStringSettings = ConfigurationManager.ConnectionStrings[config["connectionStringName"]];
            if (ConnectionStringSettings == null || ConnectionStringSettings.ConnectionString.Trim() == "")
                throw new ProviderException("Connection string cannot be blank.");

            connectionString = ConnectionStringSettings.ConnectionString;
            // Get encryption and decryption key information from the configuration.

            //Encryption skipped
            Configuration cfg =
                            WebConfigurationManager.OpenWebConfiguration(System.Web.Hosting.HostingEnvironment.ApplicationVirtualPath);
            _machineKey = (MachineKeySection)cfg.GetSection("system.web/machineKey");

            if (_machineKey.ValidationKey.Contains("AutoGenerate"))
            {
                if (PasswordFormat != MembershipPasswordFormat.Clear)
                    throw new ProviderException("Hashed or Encrypted passwords are not supported with auto-generated keys.");
            }

            // create our Fluent NHibernate session factory
            _sessionFactory = SessionHelper.CreateSessionFactory(connectionString);
            //private static ISessionFactory _sessionFactory2 =_sessionFactory;

        }

        // Change password for a user
        /// <summary>
        /// 更新用户密码
        /// </summary>
        /// <param name="username"></param>
        /// <param name="oldPwd"></param>
        /// <param name="newPwd"></param>
        /// <returns></returns>
        public override bool ChangePassword(string username, string oldPwd, string newPwd)
        {
            /*
             int rowsAffected = 0;
            Model.Users usr = null;
            if (!ValidateUser(username, oldPwd))
                return false;

            ValidatePasswordEventArgs args = new ValidatePasswordEventArgs(username, newPwd, true);

            OnValidatingPassword(args);

            if (args.Cancel)
                if (args.FailureInformation != null)
                    throw args.FailureInformation;
                else
                    throw new MembershipPasswordException("Change password canceled due to new password validation failure.");

            using (ISession session = SessionFactory.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {

                    try
                    {
                        usr = GetUserByUsername(username);

                        if (usr != null)
                        {
                            usr.Password = EncodePassword(newPwd);
                            usr.LastPasswordChangedDate = System.DateTime.Now;
                            session.Update(usr);
                            transaction.Commit();
                            rowsAffected = 1;
                        }
                    }
                    catch (Exception e)
                    {
                        if (WriteExceptionsToEventLog)
                            WriteToEventLog(e, "ChangePassword");
                        throw new ProviderException(exceptionMessage);
                    }
                }
            }

            if (rowsAffected > 0)
                return true;
            */
            return false;
        }

        // Change Password Question And Answer for a user
        /// <summary>
        /// 更新用户的密码提示问题和答案
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="newPwdQuestion"></param>
        /// <param name="newPwdAnswer"></param>
        /// <returns></returns>
        public override bool ChangePasswordQuestionAndAnswer(string username,
                     string password,
                     string newPwdQuestion,
                     string newPwdAnswer)
        {
            /*
             Model.Users usr = null;
            int rowsAffected = 0;
            if (!ValidateUser(username, password))
                return false;

            using (ISession session = SessionFactory.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        usr = GetUserByUsername(username);
                        if (usr != null)
                        {
                            usr.PasswordQuestion = newPwdQuestion;
                            usr.PasswordAnswer = newPwdAnswer;
                            session.Update(usr);
                            transaction.Commit();
                            rowsAffected = 1;
                        }
                    }
                    catch (OdbcException e)
                    {
                        if (WriteExceptionsToEventLog)
                            WriteToEventLog(e, "ChangePasswordQuestionAndAnswer");
                        throw new ProviderException(exceptionMessage);
                    }
                }
            }

            if (rowsAffected > 0)
                return true;
            */

            return false;
        }

        // Create a new Membership user 
        /// <summary>
        /// 创建Membership用户
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="email"></param>
        /// <param name="passwordQuestion"></param>
        /// <param name="passwordAnswer"></param>
        /// <param name="isApproved"></param>
        /// <param name="providerUserKey"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        public override MembershipUser CreateUser(string username,
                                                   string password,
                                                   string email,
                                                   string passwordQuestion,
                                                   string passwordAnswer,
                                                   bool isApproved,
                                                   object providerUserKey,
                                                   out MembershipCreateStatus status)
        {
            ValidatePasswordEventArgs args = new ValidatePasswordEventArgs(username, password, true);

            OnValidatingPassword(args);

            if (args.Cancel)
            {
                status = MembershipCreateStatus.InvalidPassword;
                return null;
            }

            if (RequiresUniqueEmail && GetUserNameByEmail(email) != "")
            {
                status = MembershipCreateStatus.DuplicateEmail;
                return null;
            }

            MembershipUser u = GetUser(username, false);

            /*
             if (u == null)
            {
                DateTime createDate = DateTime.Now;

                //provider user key in our case is auto int
                using (ISession session = SessionFactory.OpenSession())
                {
                    using (ITransaction transaction = session.BeginTransaction())
                    {
                        Model.Users user = new Model.Users();
                        user.UserName = username;
                        user.Password = EncodePassword(password);
                        user.Email = email;
                        user.PasswordQuestion = passwordQuestion;
                        user.PasswordAnswer = string.IsNullOrEmpty(passwordAnswer) ? string.Empty : EncodePassword(passwordAnswer);
                        user.IsApproved = isApproved;
                        user.Comment = "";
                        user.CreationDate = createDate;
                        user.LastPasswordChangedDate = createDate;
                        user.LastActivityDate = createDate;
                        user.ApplicationName = _applicationName;
                        user.IsLockedOut = false;
                        user.LastLockedOutDate = createDate;
                        user.FailedPasswordAttemptCount = 0;
                        user.FailedPasswordAttemptWindowStart = createDate;
                        user.FailedPasswordAnswerAttemptCount = 0;
                        user.FailedPasswordAnswerAttemptWindowStart = createDate;
                        try
                        {
                            int retId = (int)session.Save(user);
                            transaction.Commit();
                            if ((retId < 1))
                                status = MembershipCreateStatus.UserRejected;
                            else
                                status = MembershipCreateStatus.Success;
                        }
                        catch (Exception e)
                        {
                            status = MembershipCreateStatus.ProviderError;
                            if (WriteExceptionsToEventLog)
                                WriteToEventLog(e, "CreateUser");
                        }
                    }
                }

                //retrive and return user by user name
                return GetUser(username, false);
            }
            else
                status = MembershipCreateStatus.DuplicateUserName;
            */
            status = MembershipCreateStatus.DuplicateEmail;
            return null;
        }

        // Delete a user 
        /// <summary>
        /// 删除用户
        /// </summary>
        /// <param name="username"></param>
        /// <param name="deleteAllRelatedData"></param>
        /// <returns></returns>
        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            int rowsAffected = 0;
            /*
              Model.Users usr = null;
              using (ISession session = SessionFactory.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        usr = GetUserByUsername(username);
                        if (usr != null)
                        {
                            session.Delete(usr);
                            transaction.Commit();
                            rowsAffected = 1;
                        }
                    }
                    catch (Exception e)
                    {
                        if (WriteExceptionsToEventLog)
                            WriteToEventLog(e, "DeleteUser");
                        throw new ProviderException(exceptionMessage);
                    }
                }
            }

            if (rowsAffected > 0)
                return true;
            */
            return false;
        }

        // Get all users in db
        /// <summary>
        /// 获取数据源中的所有用户的集合
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <returns></returns>
        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            MembershipUserCollection users = new MembershipUserCollection();
            totalRecords = 0;
            /*
             IList<Model.Users> allusers = null;
            int counter = 0;
            int startIndex = pageSize * pageIndex;
            int endIndex = startIndex + pageSize - 1;

            using (ISession session = SessionFactory.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        totalRecords = (Int32)session.CreateCriteria(typeof(Model.Users))
                                    .Add(NHibernate.Criterion.Restrictions.Eq("ApplicationName", this.ApplicationName))
                                    .SetProjection(NHibernate.Criterion.Projections.Count("Id")).UniqueResult();

                        if (totalRecords <= 0) { return users; }

                        allusers = GetUsers();
                        foreach (Model.Users u in allusers)
                        {
                            if (counter >= endIndex)
                                break;
                            if (counter >= startIndex)
                            {
                                MembershipUser mu = GetMembershipUserFromUser(u);
                                users.Add(mu);
                            }
                            counter++;
                        }
                    }
                    catch (Exception e)
                    {
                        if (WriteExceptionsToEventLog)
                            WriteToEventLog(e, "GetAllUsers");
                        throw new ProviderException(exceptionMessage);
                    }
                }
            }
            */
            return users;
        }

        // Gets a number of online users
        /// <summary>
        /// 获取当前访问该应用程序的用户数
        /// </summary>
        /// <returns></returns>
        public override int GetNumberOfUsersOnline()
        {
            TimeSpan onlineSpan = new TimeSpan(0, System.Web.Security.Membership.UserIsOnlineTimeWindow, 0);
            DateTime compareTime = DateTime.Now.Subtract(onlineSpan);
            int numOnline = 0;
            /*
            using (ISession session = SessionFactory.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        numOnline = (Int32)session.CreateCriteria(typeof(Model.Users))
                                           .Add(NHibernate.Criterion.Restrictions.Eq("ApplicationName", this.ApplicationName))
                                           .Add(NHibernate.Criterion.Restrictions.Gt("LastActivityDate", compareTime))
                                           .SetProjection(NHibernate.Criterion.Projections.Count("Id")).UniqueResult();
                    }
                    catch (Exception e)
                    {
                        if (WriteExceptionsToEventLog)
                            WriteToEventLog(e, "GetNumberOfUsersOnline");
                        throw new ProviderException(exceptionMessage);
                    }
                }
            }
            */
            return numOnline;
        }

        // Get a password fo a user
        /// <summary>
        /// 通过用户名和密码问题答案获取密码
        /// </summary>
        /// <param name="username"></param>
        /// <param name="answer"></param>
        /// <returns></returns>
        public override string GetPassword(string username, string answer)
        {
            string password = string.Empty;
            string passwordAnswer = string.Empty;

            if (!EnablePasswordRetrieval)
                throw new ProviderException("Password Retrieval Not Enabled.");

            if (PasswordFormat == MembershipPasswordFormat.Hashed)
                throw new ProviderException("Cannot retrieve Hashed passwords.");
            /*
             try
            {
                Model.Users usr = GetUserByUsername(username);

                if (usr == null)
                    throw new MembershipPasswordException("The supplied user name is not found.");
                else
                {
                    if (usr.IsLockedOut)
                        throw new MembershipPasswordException("The supplied user is locked out.");

                    password = usr.Password;
                    passwordAnswer = usr.PasswordAnswer;

                }
            }
            catch (Exception e)
            {
                if (WriteExceptionsToEventLog)
                    WriteToEventLog(e, "GetPassword");
                throw new ProviderException(exceptionMessage);
            }

            if (RequiresQuestionAndAnswer && !CheckPassword(answer, passwordAnswer))
            {
                UpdateFailureCount(username, "passwordAnswer");
                throw new MembershipPasswordException("Incorrect password answer.");
            }

            if (PasswordFormat == MembershipPasswordFormat.Encrypted)
               {
                   password = UnEncodePassword(password);
               }
            */

            return password;
        }

        // Get a membership user by username
        /// <summary>
        /// 通过用户名获取成员资格用户信息
        /// </summary>
        /// <param name="username"></param>
        /// <param name="userIsOnline"></param>
        /// <returns></returns>
        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            //return GetMembershipUserByKeyOrUser(false, username, 0, userIsOnline);
            return null;
        }

        //  Get a membership user by key ( in our case key is int)
        /// <summary>
        /// 根据成员资格用户的唯一标识符从数据源获取用户信息
        /// </summary>
        /// <param name="providerUserKey"></param>
        /// <param name="userIsOnline"></param>
        /// <returns></returns>
        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            //return GetMembershipUserByKeyOrUser(true, string.Empty, providerUserKey, userIsOnline);
            return null;
        }

        //Unlock a user given a username 
        /// <summary>
        /// 清除锁定，以便可以验证该成员资格用户
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public override bool UnlockUser(string username)
        {           
            bool unlocked = false;
            /*
            Model.Users usr = null;
            using (ISession session = SessionFactory.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {

                    try
                    {
                        usr = GetUserByUsername(username);

                        if (usr != null)
                        {
                            usr.LastLockedOutDate = System.DateTime.Now;
                            usr.IsLockedOut = false;
                            session.Update(usr);
                            transaction.Commit();
                            unlocked = true;
                        }
                    }
                    catch (Exception e)
                    {
                        WriteToEventLog(e, "UnlockUser");
                        throw new ProviderException(exceptionMessage);
                    }
                }
            }
            */
            return unlocked;
        }

        //Gets a membehsip user by email
        /// <summary>
        /// 获取与指定的电子邮件地址关联的用户名
        /// </summary>
        /// <param name="email"></param>
        /// <returns></returns>
        public override string GetUserNameByEmail(string email)
        {
            /*
             Model.Users usr = null;
            using (ISession session = SessionFactory.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {

                    try
                    {
                        usr = session.CreateCriteria(typeof(Model.Users))
                                            .Add(NHibernate.Criterion.Restrictions.Eq("Email", email))
                                            .UniqueResult<Model.Users>();
                    }
                    catch (Exception e)
                    {
                        if (WriteExceptionsToEventLog)
                            WriteToEventLog(e, "GetUserNameByEmail");
                        throw new ProviderException(exceptionMessage);
                    }
                }
            }
            if (usr == null)
                return string.Empty;
            else
                return usr.UserName; 
            */
            return null;
        }

        // Reset password for a user
        /// <summary>
        /// 将用户密码重置为一个自动生成的新密码
        /// </summary>
        /// <param name="username"></param>
        /// <param name="answer"></param>
        /// <returns></returns>
        public override string ResetPassword(string username, string answer)
        {
            int rowsAffected = 0;
            /*
             Model.Users usr = null;

            if (!EnablePasswordReset)
                throw new NotSupportedException("Password reset is not enabled.");

            if (answer == null && RequiresQuestionAndAnswer)
            {
                UpdateFailureCount(username, "passwordAnswer");
                throw new ProviderException("Password answer required for password reset.");
            }

            string newPassword =
                            System.Web.Security.Membership.GeneratePassword(newPasswordLength, MinRequiredNonAlphanumericCharacters);


            ValidatePasswordEventArgs args = new ValidatePasswordEventArgs(username, newPassword, true);

            OnValidatingPassword(args);

            if (args.Cancel)
                if (args.FailureInformation != null)
                    throw args.FailureInformation;
                else
                    throw new MembershipPasswordException("Reset password canceled due to password validation failure.");

            string passwordAnswer = "";

            using (ISession session = SessionFactory.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        usr = GetUserByUsername(username);
                        if (usr == null)
                            throw new MembershipPasswordException("The supplied user name is not found.");

                        if (usr.IsLockedOut)
                            throw new MembershipPasswordException("The supplied user is locked out.");

                        if (RequiresQuestionAndAnswer && !CheckPassword(answer, passwordAnswer))
                        {
                            UpdateFailureCount(username, "passwordAnswer");
                            throw new MembershipPasswordException("Incorrect password answer.");
                        }

                        usr.Password = EncodePassword(newPassword);
                        usr.LastPasswordChangedDate = System.DateTime.Now;
                        usr.UserName = username;
                        usr.ApplicationName = this.ApplicationName;
                        session.Update(usr);
                        transaction.Commit();
                        rowsAffected = 1;
                    }
                    catch (OdbcException e)
                    {
                        if (WriteExceptionsToEventLog)
                            WriteToEventLog(e, "ResetPassword");
                        throw new ProviderException(exceptionMessage);
                    }
                }
            }
            if (rowsAffected > 0)
                return newPassword;
            else
                throw new MembershipPasswordException("User not found, or user is locked out. Password not Reset.");
            */
            return null;
        }

        // Update a user information 
        /// <summary>
        /// 更新数据源中有关用户的信息
        /// </summary>
        /// <param name="user"></param>
        public override void UpdateUser(MembershipUser user)
        {
            /*
             Model.Users usr = null;
            using (ISession session = SessionFactory.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        usr = GetUserByUsername(user.UserName);
                        if (usr != null)
                        {
                            usr.Email = user.Email;
                            usr.Comment = user.Comment;
                            usr.IsApproved = user.IsApproved;
                            session.Update(usr);
                            transaction.Commit();
                        }
                    }
                    catch (Exception e)
                    {
                        if (WriteExceptionsToEventLog)
                        {
                            WriteToEventLog(e, "UpdateUser");
                            throw new ProviderException(exceptionMessage);
                        }
                    }
                }
            }
            */
        }

        // Validates as user
        /// <summary>
        /// 验证数据源中是否存在指定的用户名和密码
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public override bool ValidateUser(string username, string password)
        {
            bool isValid = false;
            /*
            Model.Users usr = null;
            using (ISession session = SessionFactory.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        usr = GetUserByUsername(username);
                        if (usr == null)
                            return false;
                        if (usr.IsLockedOut)
                            return false;

                        if (CheckPassword(password, usr.Password))
                        {
                            if (usr.IsApproved)
                            {
                                isValid = true;
                                usr.LastLoginDate = DateTime.Now;
                                usr.LastActivityDate = DateTime.Now;
                                session.Update(usr);
                                transaction.Commit();
                            }
                        }
                        else
                            UpdateFailureCount(username, "password");
                    }
                    catch (Exception e)
                    {
                        if (WriteExceptionsToEventLog)
                        {
                            WriteToEventLog(e, "ValidateUser");
                            throw new ProviderException(exceptionMessage);
                        }
                        else
                            throw e;
                    }
                }
            }
            */

            return isValid;
        }

        // Find users by a name, note : does not do a like search
        /// <summary>
        /// 获取与指定用户名匹配的成员资格用户的集合
        /// </summary>
        /// <param name="usernameToMatch"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <returns></returns>
        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            MembershipUserCollection users = new MembershipUserCollection();

            /*
             IList<Model.Users> allusers = null;            
            int counter = 0;
            int startIndex = pageSize * pageIndex;
            int endIndex = startIndex + pageSize - 1;
            totalRecords = 0;

            using (ISession session = SessionFactory.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        allusers = GetUsersLikeUserName(usernameToMatch);
                        if (allusers == null)
                            return users;
                        if (allusers.Count > 0)
                            totalRecords = allusers.Count;
                        else
                            return users;

                        foreach (Model.Users u in allusers)
                        {
                            if (counter >= endIndex)
                                break;
                            if (counter >= startIndex)
                            {
                                MembershipUser mu = GetMembershipUserFromUser(u);
                                users.Add(mu);
                            }
                            counter++;
                        }
                    }
                    catch (Exception e)
                    {
                        if (WriteExceptionsToEventLog)
                        {
                            WriteToEventLog(e, "FindUsersByName");
                            throw new ProviderException(exceptionMessage);
                        }
                        else
                            throw e;
                    }

                }
            }
            */
            totalRecords = 0;
            return users;
        }

        // Search users by email , NOT a Like match
        /// <summary>
        /// 获取一个成员资格用户的集合，其中的电子邮件地址包含要匹配的指定电子邮件地址
        /// </summary>
        /// <param name="emailToMatch"></param>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <param name="totalRecords"></param>
        /// <returns></returns>
        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            MembershipUserCollection users = new MembershipUserCollection();
            /*
             IList<Model.Users> allusers = null;            
            int counter = 0;
            int startIndex = pageSize * pageIndex;
            int endIndex = startIndex + pageSize - 1;
            totalRecords = 0;

            using (ISession session = SessionFactory.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        allusers = GetUsersLikeEmail(emailToMatch);
                        if (allusers == null)
                            return users;
                        if (allusers.Count > 0)
                            totalRecords = allusers.Count;
                        else
                            return users;

                        foreach (Model.Users u in allusers)
                        {
                            if (counter >= endIndex)
                                break;
                            if (counter >= startIndex)
                            {
                                MembershipUser mu = GetMembershipUserFromUser(u);
                                users.Add(mu);
                            }
                            counter++;
                        }
                    }
                    catch (Exception e)
                    {
                        if (WriteExceptionsToEventLog)
                        {
                            WriteToEventLog(e, "FindUsersByEmail");
                            throw new ProviderException(exceptionMessage);
                        }
                        else
                            throw e;
                    }
                }
            }
            */
            totalRecords = 0;
            return users;
        }
        /*
        public MembershipUserCollection GetAllOnlineUsers()
        {
            TimeSpan onlineSpan = new TimeSpan(0, System.Web.Security.Membership.UserIsOnlineTimeWindow, 0);
            DateTime compareTime = DateTime.Now.Subtract(onlineSpan);
            MembershipUserCollection usersCollection=new MembershipUserCollection();
            IList<Model.Users> users;
            using (ISession session = SessionFactory.OpenSession())
            {
                using (ITransaction transaction = session.BeginTransaction())
                {
                    try
                    {
                        users = session.CreateCriteria(typeof(Model.Users))
                                .Add(NHibernate.Criterion.Restrictions.Eq("ApplicationName", this.ApplicationName))
                                .Add(NHibernate.Criterion.Restrictions.Gt("LastActivityDate", compareTime))
                                .List<Model.Users>();
                        foreach (Model.Users user in users) {
                            MembershipUser mu = GetMembershipUserFromUser(user);
                            usersCollection.Add(mu);
                        }
                    }
                    catch (Exception e)
                    {
                        if (WriteExceptionsToEventLog)
                            WriteToEventLog(e, "GetAllOnlineUsers");
                        throw new ProviderException(exceptionMessage);
                    }
                }
            }
            return usersCollection;
        }
         */
        #endregion
    }
}
