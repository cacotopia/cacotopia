﻿#region Using

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using FluentNHibernate.Conventions.Helpers;

#endregion 

namespace BlogEngine.Core.Providers
{
    public static class SessionHelper
    {
        public static ISessionFactory CreateSessionFactory(string connectstring)
        {
            return

                Fluently.Configure()
                .Database(FluentNHibernate.Cfg.Db.MsSqlConfiguration.MsSql2012
                .ConnectionString(connectstring)
                )
                .Mappings(m =>
                {
                    m.FluentMappings.AddFromAssemblyOf<BlogEngine.Core.Providers.DbRoleProvider>()
                     .Conventions.Add(
                     Table.Is(x => "be_" + x.EntityType)
                     //PrimaryKey.Name.Is(x => "ID"),
                     //DefaultLazy.Always(),
                     //ForeignKey.EndsWith("ID"),
                     //AutoImport.Always(),
                     //DefaultAccess.Property(),
                     //DefaultCascade.All(),
                     //DynamicInsert.AlwaysTrue(),
                     //DynamicUpdate.AlwaysTrue(),
                     //OptimisticLock.Is(x=>x.Dirty()),
                     //Cache.Is(x=>x.ReadOnly())
                     );                                       
                }                
                )
                .BuildSessionFactory();
        }
    }
}
