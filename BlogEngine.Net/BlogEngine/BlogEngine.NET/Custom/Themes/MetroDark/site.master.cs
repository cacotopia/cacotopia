using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BlogEngine.Core;
using System.Web.UI.HtmlControls;

public partial class themes_MetroDark_site : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Security.IsAuthenticated)
        {
            aUser.InnerText = Page.User.Identity.Name;
            aLogin.InnerText = Resources.labels.logoff;
            aLogin.HRef = Utils.RelativeWebRoot + "Account/login.aspx?logoff";
        }
        else
        {
            aLogin.HRef = Utils.RelativeWebRoot + "Account/login.aspx";
            aLogin.InnerText = Resources.labels.login;
        }
    }

    /*
    protected override void OnInit(EventArgs e)
    {
        base.OnInit(e);
        RegisterScriptFile("jquery-ui-1.8.16.custom.min.js");
    }

    private void RegisterScriptFile(string script)
    {
        const string scriptSourceTemplate = "{0}js.axd?path={0}Custom/ThemesMetroDark/scripts/{1}";

        var scriptTag = new HtmlGenericControl("script");
        scriptTag.Attributes.Add("src", string.Format(scriptSourceTemplate, ResolveUrl("~/"), script));
        scriptTag.Attributes.Add("type", "text/javascript");

        Page.Header.Controls.Add(scriptTag);
    }
     * */
}
