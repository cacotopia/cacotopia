/* 
Author: rtur (http://rtur.net)
Showcase for BlogEngine.NET
*/

using System;
using System.IO;
using BlogEngine.Core;
using BlogEngine.Core.Web.Extensions;
using System.Data;
using System.Drawing;
using Admin;

namespace rtur.net.Showcase
{
    public partial class Admin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            App_Code.WebUtils.CheckIfPrimaryBlog(false);

            if (!Page.IsPostBack)
            {
                if (Request.QueryString["id"] != null)
                    DeleteItem(Request.QueryString["id"]);

                if (Request.QueryString["ctrl"] != null)
                    txtUrl.Text = Request.QueryString["ctrl"];

                if (Request.QueryString["scs"] != null)
                    ((AdminMasterPage)Master).SetStatus("success", "Image saved");
            }
        }

        protected void SaveItem(object sender, EventArgs e)
        {
            try
            {
                Upload();

                var src = txtUploadImage.FileName.ToLowerInvariant();

                Settings.ImageData.AddValues(new[] { src, txtUrl.Text, txtTitle.Text });

                ExtensionManager.SaveSettings(Constants.ExtensionName, Settings.ImageData);
            }
            catch (Exception ex)
            {
                Utils.Log("rtur.net.Showcase.Admin.SaveItem", ex);
                ((AdminMasterPage)Master).SetStatus("warning", ex.Message);
            }
            Response.Redirect(Request.RawUrl);
        }

        protected void EditItem(object sender, EventArgs e)
        {
            try
            {
                ImgItem oldItem = new ImgItem();
                ImgItem newItem = new ImgItem();

                newItem.Image = txtUploadImage.FileName.ToLowerInvariant();
                newItem.Url = txtUrl.Text;
                newItem.Title = txtTitle.Text;

                oldItem.Image = hdnEditId.Value;

                if (Settings.ImageData != null)
                {
                    var table = Settings.ImageData.GetDataTable();
                    string uid, title;
                    int cnt = 0;
                    foreach (DataRow row in table.Rows)
                    {
                        uid = (string)row[0];
                        title = (string)row[1];

                        if (uid == oldItem.Image)
                        {
                            if (!string.IsNullOrEmpty(newItem.Image) && newItem.Image != oldItem.Image)
                                Upload();
                            else
                                newItem.Image = oldItem.Image;

                            Settings.ImageData.Parameters[0].Values[cnt] = newItem.Image;
                            Settings.ImageData.Parameters[1].Values[cnt] = newItem.Url;
                            Settings.ImageData.Parameters[2].Values[cnt] = newItem.Title;

                            ExtensionManager.SaveSettings(Constants.ExtensionName, Settings.ImageData);
                            break;
                        }
                        cnt++;
                    }
                }
            }
            catch (Exception ex)
            {
                Utils.Log("rtur.net.Showcase.Admin.EditItem", ex);
                ((AdminMasterPage)Master).SetStatus("warning", ex.Message);
            }
            var msgUrl = Request.RawUrl.Contains("?") ? "&scs=y" : "?scs=y";
            Response.Redirect(Request.RawUrl + msgUrl);
        }

        protected void DeleteItem(string id)
        {
            try
            {
                var table = Settings.ImageData.GetDataTable();

                for (int i = 0; i < table.Rows.Count; i++)
                {
                    if (table.Rows[i][Constants.Img].ToString() == id)
                    {
                        foreach (ExtensionParameter par in Settings.ImageData.Parameters)
                        {
                            par.DeleteValue(i);
                        }
                        ExtensionManager.SaveSettings(Constants.ExtensionName, Settings.ImageData);
                        break;
                    }
                }
                // delete image
                var folder = Server.MapPath(Utils.ApplicationRelativeWebRoot + Constants.ImageFolder);
                var imgPath = Path.Combine(folder, id);
                File.Delete(imgPath);

                // delete thumbnail
                var fInfo = new System.IO.FileInfo(imgPath);
                var thumbName = imgPath.Replace(fInfo.Extension, "_thumb" + fInfo.Extension);

                if (File.Exists(thumbName))
                    File.Delete(thumbName);

                ((AdminMasterPage)Master).SetStatus("success", "Image deleted");
            }
            catch (Exception ex)
            {
                Utils.Log("rtur.net.Showcase.Admin.DeleteItem", ex);
                ((AdminMasterPage)Master).SetStatus("warning", ex.Message);
            }
        }

        private void Upload()
        {
            var folder = Server.MapPath(Utils.ApplicationRelativeWebRoot + Constants.ImageFolder);
            var fileName = Path.Combine(folder, txtUploadImage.FileName);
            
            if (!Directory.Exists(folder))
                Directory.CreateDirectory(folder);

            if (File.Exists(fileName))
                File.Delete(fileName);

            txtUploadImage.PostedFile.SaveAs(fileName);

            // generate thumbnail
            var fInfo = new System.IO.FileInfo(fileName);
            var thumbName = fileName.Replace(fInfo.Extension, "_thumb" + fInfo.Extension);

            if (File.Exists(thumbName))
                File.Delete(thumbName);

            Image image = Image.FromFile(fileName);
            Image thumb = image.GetThumbnailImage(75, 75, () => false, IntPtr.Zero);

            thumb.Save(thumbName);
        }
    }

    class ImgItem
    {
        public string Image { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
    }
}
