<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/admin/admin.master" CodeFile="Admin.aspx.cs" Inherits="rtur.net.Showcase.Admin" %>

<%@ MasterType VirtualPath="~/admin/admin.master" %>

<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="rtur.net.Showcase" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphAdmin" Runat="Server">
<div class="content-box-outer">
    <div class="content-box-full">
        
        <div class="info" style="float: right; width: 600px; position: relative; top: 35px">
            <h4>Showcase Help</h4>
            <p>To use showcase, first register control in web.config :</p>
			<pre>&lt;add assembly="Showcase" namespace="rtur.net.Showcase" tagPrefix="Sc" /&gt;</pre>
			<p>Then add it to site.master in your theme :</p>
			<pre>&lt;Sc:ShowcaseControl ID="showcase" Width="970" Height="300" MaxItems="3" runat="server" /&gt;</pre>
        </div>
        
        <div>
            <h1>Settings: Showcase</h1>
            <div style="float: left; width: 500px">
                <p>
                    <span style="width: 120px; display:inline-block;">Image Title:</span>
                    <asp:TextBox ID="txtTitle" Width="300" runat="server" MaxLength="250"></asp:TextBox>
                </p>
                <p>
                    <span style="width: 120px; display:inline-block;">Url:</span>
                    <asp:TextBox ID="txtUrl" runat="server" MaxLength="200"></asp:TextBox>
                </p>
                <p>
                    <span style="width: 120px; display:inline-block;"><%=Resources.labels.upload %>:</span>
                    <asp:FileUpload runat="server" id="txtUploadImage" />
                </p>
                <p>
                    <span style="width: 120px; display:inline-block;">&nbsp;</span>
                    <asp:button runat="server" ValidationGroup="data" CssClass="btn primary" id="btnSave" Text=" Add " OnClick="SaveItem" /> 
                    <asp:button runat="server" ValidationGroup="data" CssClass="btn primary" id="btnEdit" Text=" Update " OnClick="EditItem" /> 
                    <a id="btnCancel" href="" onclick="return CancelSliderEdit()">Cancel</a>    
                </p>
                <asp:HiddenField ID="hdnEditId" runat="server" />
            </div>

            <div class="clear"></div>
            
                        <div>
                <table id="RoleService" class="beTable rounded">
                  <thead>
                    <tr>
                      <th width="25">&nbsp;</th>
                      <th width="200">Image File</th>
	                  <th width="250">Title</th>
                      <th width="120">Url</th>
                      <th width="80">&nbsp;</th>
                      <th width="80">&nbsp;</th>
                    </tr>
                  </thead>
                  <tbody>
                    <%
                    var i = 0;
                    var root = BlogEngine.Core.Utils.RelativeWebRoot;
                    foreach (DataRow row in Settings.ImageData.GetDataTable().Rows)
                    {
                        var cls = i%2 == 0 ? "" : "alt";
                        string img = row[Constants.Img].ToString();
                        if (string.IsNullOrEmpty(img)) continue;

                        string title = row[Constants.Title].ToString();
                        string url = row[Constants.Url].ToString();

                        var editPars = string.Format("{0}|{1}|{2}", row[Constants.Img], row[Constants.Title], row[Constants.Url]);
                    %>
                    <tr class="<%=cls %>">
                      <td><img src="<%=root %>Custom/Controls/Showcase/Images/<%=img.Replace(".", "_thumb.") %>" width="24" height="24" alt=""/></td>
                      <td><%= img %></td>
                      <td><%= title %></td>
	                  <td><%= url %></td>
                      <td align="center" style="vertical-align:middle"><a class="editAction" href="" onclick="return EditSlider('<%=row[0] %>', '<%=editPars %>');">Edit</a></td>
                      <td align="center" style="vertical-align:middle"><a class="deleteAction" href="?id=<%=row[Constants.Img].ToString() %>">Delete</a></td>
                    </tr>
                    <% i++; } %>
                  </tbody>
                </table>
            </div>
        
        </div>
    </div>
</div>
<script type="text/javascript">
    CancelSliderEdit();
    function EditSlider(id, pars) {
        var n = pars.split('|');
        $("[id$='_hdnEditId']").val(id);
        for (var i = 0; i < n.length; i++) {
            if (i == 1) { $("[id$='_txtTitle']").val(n[i]); }
            if (i == 2) { $("[id$='_txtUrl']").val(n[i]); }
        }
        $("[id$='_btnSave']").hide();
        $("[id$='_btnEdit']").show();
        $("#btnCancel").show();
        return false;
    }
    function CancelSliderEdit() {
        $("[id$='_txtUrl']").val('');
        $("[id$='_txtTitle']").val('');
        $("[id$='_btnSave']").show();
        $("[id$='_btnEdit']").hide();
        $("#btnCancel").hide();
        return false;
    }
</script>
</asp:Content>
