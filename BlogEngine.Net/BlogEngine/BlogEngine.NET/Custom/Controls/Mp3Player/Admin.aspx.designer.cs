﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


    public partial class audio_Admin
    {

        protected System.Web.UI.HtmlControls.HtmlGenericControl ErrorMsg;

        protected System.Web.UI.HtmlControls.HtmlGenericControl InfoMsg;

        protected System.Web.UI.WebControls.Literal litPlayer;

        protected System.Web.UI.WebControls.TextBox Contrlo_background;

        protected System.Web.UI.WebControls.Label Label2;

        protected System.Web.UI.HtmlControls.HtmlGenericControl lblBg;

        protected System.Web.UI.WebControls.TextBox Player_background;

        protected System.Web.UI.WebControls.Label Label14;

        protected System.Web.UI.HtmlControls.HtmlGenericControl lblLeftBg;

        protected System.Web.UI.WebControls.TextBox Left_background;

        protected System.Web.UI.WebControls.Label Label15;        

        protected System.Web.UI.HtmlControls.HtmlGenericControl lblRightBg;

        protected System.Web.UI.WebControls.TextBox Right_background;

        protected System.Web.UI.WebControls.Label Label1;

        protected System.Web.UI.HtmlControls.HtmlGenericControl lblRightBgHvr;

        protected System.Web.UI.WebControls.TextBox Right_background_hover;

        protected System.Web.UI.WebControls.Label Label3;

        protected System.Web.UI.HtmlControls.HtmlGenericControl lblLeftIcon;

        protected System.Web.UI.WebControls.TextBox Left_icon;

        protected System.Web.UI.WebControls.Label Label16;

        protected System.Web.UI.HtmlControls.HtmlGenericControl lblRightIcon;

        protected System.Web.UI.WebControls.TextBox Right_icon;

        protected System.Web.UI.WebControls.Label Label17;

        protected System.Web.UI.HtmlControls.HtmlGenericControl lblRightIconHvr;

        protected System.Web.UI.WebControls.TextBox Right_icon_hover;

        protected System.Web.UI.WebControls.Label Label18;

        protected System.Web.UI.HtmlControls.HtmlGenericControl lblText;

        protected System.Web.UI.WebControls.TextBox Text_color;

        protected System.Web.UI.WebControls.Label Label19;

        protected System.Web.UI.HtmlControls.HtmlGenericControl lblSlider;

        protected System.Web.UI.WebControls.TextBox Slider;

        protected System.Web.UI.WebControls.Label Label20;

        protected System.Web.UI.HtmlControls.HtmlGenericControl lblLoader;

        protected System.Web.UI.WebControls.TextBox Loader;

        protected System.Web.UI.WebControls.Label Label21;

        protected System.Web.UI.HtmlControls.HtmlGenericControl lblTrack;

        protected System.Web.UI.WebControls.TextBox Track;

        protected System.Web.UI.WebControls.Label Label22;

        protected System.Web.UI.HtmlControls.HtmlGenericControl lblBoarder;

        protected System.Web.UI.WebControls.TextBox Border;

        protected System.Web.UI.WebControls.Label Label23;

        protected System.Web.UI.HtmlControls.HtmlGenericControl Label4;

        protected System.Web.UI.WebControls.TextBox Control_width;

        protected System.Web.UI.WebControls.Label Label5;

        protected System.Web.UI.HtmlControls.HtmlGenericControl Label6;

        protected System.Web.UI.WebControls.TextBox Control_height;

        protected System.Web.UI.WebControls.Label Label7;

        protected System.Web.UI.WebControls.Button btnSave;

        protected System.Web.UI.HtmlControls.HtmlGenericControl formContainer;

        protected System.Web.UI.HtmlControls.HtmlGenericControl lblBgColor;

    }
