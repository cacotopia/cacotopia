using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using BlogEngine.Core.Web.Extensions;
using System.Text.RegularExpressions;
using App_Code.Extensions;

public partial class audio_Admin : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ErrorMsg.InnerHtml = string.Empty;
        ErrorMsg.Visible = false;

        if (!Page.IsPostBack)
        {
            Mp3Player.SetDefaultSettings();
            BindForm();
        }

        SetPlayer();
    }

    protected void BtnSaveClick(object sender, EventArgs e)
    {
        if (IsValidForm())
        {
            SaveSettings();
        }
    }

    protected void BindForm()
    {
        btnSave.Text = Resources.labels.saveSettings;

        Control_width.Text = Mp3Player.Settings.GetSingleValue(Mp3Player.Width);
        Control_height.Text = Mp3Player.Settings.GetSingleValue(Mp3Player.Height);
        Contrlo_background.Text = Mp3Player.Settings.GetSingleValue(Mp3Player.BgColor);
        Player_background.Text = Mp3Player.Settings.GetSingleValue(Mp3Player.Bg);
        Left_background.Text = Mp3Player.Settings.GetSingleValue(Mp3Player.Leftbg);
        Left_icon.Text = Mp3Player.Settings.GetSingleValue(Mp3Player.Lefticon);
        Right_background.Text = Mp3Player.Settings.GetSingleValue(Mp3Player.Rightbg);
        Right_background_hover.Text = Mp3Player.Settings.GetSingleValue(Mp3Player.Rightbghover);
        Right_icon.Text = Mp3Player.Settings.GetSingleValue(Mp3Player.Righticon);
        Right_icon_hover.Text = Mp3Player.Settings.GetSingleValue(Mp3Player.Righticonhover);
        Text_color.Text = Mp3Player.Settings.GetSingleValue(Mp3Player.Text);
        Slider.Text = Mp3Player.Settings.GetSingleValue(Mp3Player.Slider);
        Track.Text = Mp3Player.Settings.GetSingleValue(Mp3Player.Track);
        Border.Text = Mp3Player.Settings.GetSingleValue(Mp3Player.Border);
        Loader.Text = Mp3Player.Settings.GetSingleValue(Mp3Player.Loader);

        lblBgColor.Style["background-color"] = "#" + Mp3Player.Settings.GetSingleValue(Mp3Player.BgColor);
        lblBg.Style["background-color"] = "#" + Mp3Player.Settings.GetSingleValue(Mp3Player.Bg);
        lblLeftBg.Style["background-color"] = "#" + Mp3Player.Settings.GetSingleValue(Mp3Player.Leftbg);
        lblLeftIcon.Style["background-color"] = "#" + Mp3Player.Settings.GetSingleValue(Mp3Player.Lefticon);
        lblRightBg.Style["background-color"] = "#" + Mp3Player.Settings.GetSingleValue(Mp3Player.Rightbg);
        lblRightBgHvr.Style["background-color"] = "#" + Mp3Player.Settings.GetSingleValue(Mp3Player.Rightbghover);
        lblRightIcon.Style["background-color"] = "#" + Mp3Player.Settings.GetSingleValue(Mp3Player.Righticon);
        lblRightIconHvr.Style["background-color"] = "#" + Mp3Player.Settings.GetSingleValue(Mp3Player.Righticonhover);
        lblText.Style["background-color"] = "#" + Mp3Player.Settings.GetSingleValue(Mp3Player.Text);
        lblSlider.Style["background-color"] = "#" + Mp3Player.Settings.GetSingleValue(Mp3Player.Slider);
        lblTrack.Style["background-color"] = "#" + Mp3Player.Settings.GetSingleValue(Mp3Player.Track);
        lblBoarder.Style["background-color"] = "#" + Mp3Player.Settings.GetSingleValue(Mp3Player.Border);
        lblLoader.Style["background-color"] = "#" + Mp3Player.Settings.GetSingleValue(Mp3Player.Loader);
    }

    protected bool IsValidForm()
    {
        foreach (Control ctl in formContainer.Controls)
        {
            if (ctl.GetType().Name == "TextBox")
            {
                var box = (TextBox)ctl;

                if (box.Text.Trim().Length == 0)
                {
                    ErrorMsg.InnerHtml = "\"" + box.ID.Replace("_", " ") + "\" is a required field";
                    ErrorMsg.Visible = true;
                    break;
                }
                else
                {
                    if (box.ID == "Control_height" || box.ID == "Control_width")
                    {
                        if (!IsInteger(box.Text))
                        {
                            ErrorMsg.InnerHtml = "\"" + box.ID.Replace("_", " ") + "\" must be a number";
                            ErrorMsg.Visible = true;
                            break;
                        }
                    }
                }
            }
        }
        return ErrorMsg.InnerHtml.Length <= 0;
    }

    protected void SaveSettings()
    {
        Mp3Player.Settings.UpdateScalarValue(Mp3Player.Width, Control_width.Text);
        Mp3Player.Settings.UpdateScalarValue(Mp3Player.Height, Control_height.Text);
        Mp3Player.Settings.UpdateScalarValue(Mp3Player.BgColor, Contrlo_background.Text);
        Mp3Player.Settings.UpdateScalarValue(Mp3Player.Bg, Player_background.Text);
        Mp3Player.Settings.UpdateScalarValue(Mp3Player.Leftbg, Left_background.Text);
        Mp3Player.Settings.UpdateScalarValue(Mp3Player.Lefticon, Left_icon.Text);
        Mp3Player.Settings.UpdateScalarValue(Mp3Player.Rightbg, Right_background.Text);
        Mp3Player.Settings.UpdateScalarValue(Mp3Player.Rightbghover, Right_background_hover.Text);
        Mp3Player.Settings.UpdateScalarValue(Mp3Player.Righticon, Right_icon.Text);
        Mp3Player.Settings.UpdateScalarValue(Mp3Player.Righticonhover, Right_icon_hover.Text);
        Mp3Player.Settings.UpdateScalarValue(Mp3Player.Text, Text_color.Text);
        Mp3Player.Settings.UpdateScalarValue(Mp3Player.Slider, Slider.Text);
        Mp3Player.Settings.UpdateScalarValue(Mp3Player.Track, Track.Text);
        Mp3Player.Settings.UpdateScalarValue(Mp3Player.Border, Border.Text);
        Mp3Player.Settings.UpdateScalarValue(Mp3Player.Loader, Loader.Text);

        ExtensionManager.SaveSettings(Mp3Player.Ext, Mp3Player.Settings);
        Response.Redirect(Request.RawUrl);
    }

    protected void SetLabelColor(Label label, string color)
    {
        label.BackColor = System.Drawing.ColorTranslator.FromHtml("#" + color);
    }

    public static bool IsInteger(string theValue)
    {
        var isNumber = new Regex(@"^\d+$");
        var m = isNumber.Match(theValue);
        return m.Success;
    }
   
    protected void SetPlayer()
    {
        Mp3Player.AddJsToTheHeader();

        var player = Mp3Player.PlayerObject("test.mp3");
        player = "<script type=\"text/javascript\">InsertPlayer(\"" + player + "\");</script>";
        litPlayer.Text = player;
    }
}
